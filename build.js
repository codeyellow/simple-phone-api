#!/usr/bin/env node
// Convert audio assets to data-URLs in a JS file
/* eslint-env node */
/* eslint max-len:[2,{"code":100,"ignoreUrls":true}]*/
'use strict';

var fs = require('fs');
var path = require('path');
// npm install mime
var spawn = require('child_process').spawn;

var baseDir = __dirname;
// Path to data, relative to baseDir
var patchesDir = 'patches';
var outputFile = path.join(baseDir, 'dist/phone.js');

var encodedFiles = {};

process.chdir(baseDir);


var phone_sounds_dot_js_content = fs.readFileSync('src/phone_sounds.js', {encoding: 'utf-8'});

// Serialize all audio files, for inline inclusion in the exported library.
// Grab the contents of the PhoneSounds.IDS array.
var file_ids = /PhoneSounds\.IDS\s*=\s*(\[[^\]]+\])/.exec(phone_sounds_dot_js_content)[1];
file_ids = file_ids.replace(/'/g, '"');
file_ids = JSON.parse(file_ids);
file_ids.forEach(function(soundId) {
    var pathToFile = 'sounds/' + soundId + '.ogg';
    // The files must exist. If they don't, Node will throw an error at the next line.
    var data = fs.readFileSync(pathToFile);
    var base64encoded = Buffer.from(data).toString('base64');
    var dataURI = 'data:audio/ogg;base64,' + base64encoded;

    encodedFiles[soundId] = dataURI;
});

encodedFiles = JSON.stringify(encodedFiles, null, 4);
var moduleContent =
    [
    ';(function(){',
    // Expose AudioContext as a singleton, to make sure that the library creates
    // only one AudioContext. This is because AudioContexts are scarce. For
    // instance, Chrome 42 allows only 6 AudioContexts to be created.
    // NOTE: This is ONLY safe because howler.js and src/phone.js creates but
    // never destroys the AudioContext (via .close()). If either libary ever
    // decides to "optimize" their use of AudioContext, then this singleton
    // "constructor" has to be revisited.
    function AudioContext() {
        if (!AudioContext.instance) {
            AudioContext.instance = new window.AudioContext();
            // Prevent Howler from destroying the singleton instance.
            // https://github.com/goldfire/howler.js/blob/9ad4eb1a973ca6faeef476315e39e938bf1547eb/src/howler.core.js#L165
            AudioContext.instance.close = undefined;
        }
        return AudioContext.instance;
    }.toString(),

    // Sound API.
    'define(\'phone_sounds_data\', ' + encodedFiles + ');',
    fs.readFileSync('howler.js', {encoding: 'utf-8'}),
    phone_sounds_dot_js_content,

    patchJsSIP(fs.readFileSync('jssip.js', {encoding: 'utf-8'})),
    fs.readFileSync('src/wrappedGetUserMedia.js', {encoding: 'utf-8'}),
    fs.readFileSync('src/phone.js', {encoding: 'utf-8'}),
    '})();',
    ''
    ].join('\n');

// Generate base file
fs.writeFileSync(outputFile, moduleContent);
// Retrieve list of patches
var patches = fs.readdirSync(patchesDir)
    .sort(function(x, y) {
        return x.toLocaleLowerCase().localeCompare(y.toLocaleLowerCase());
    })
    .filter(function(filename) {
        // Skip hidden / temporary files
        return filename.charAt(0) !== '.';
    });

// Apply all patches
(function applyPatch() {
    var patch = patches.shift();
    if (!patch) {
        return;
    }
    patch = path.join(patchesDir, patch);
    console.log('\x1b[1;32m' + 'Applying patch: ' + patch + '\x1b[0m');
    var cp = spawn('patch', ['--no-backup-if-mismatch', outputFile, patch], {
        cwd: baseDir,
        stdio: 'inherit'
    });
    cp.on('close', function(code) {
        if (code !== 0) {
            console.error('\x1b[1;31mFailed to apply ' + patch + '! ' +
                    'Patch exited with non-zero exit code (' + code + ').\x1b[0m');
            // Remove file, to avoid not noticing the error.
            fs.unlink(outputFile, function(e) {
                if (e) {
                    console.error('Failed to remove ' + outputFile + ', ' + e);
                }
                process.exit(-1);
            });
            return;
        }
        // Done, next!
        applyPatch();
    });
})();


// To generate a patch after modifying dist/phone.js during development,
// run the following command:
// git diff --no-prefix dist/phone.js > patches/description-of-patch.patch


function patchJsSIP(content) {
    // Apply patches that don't fit in the .patch format.
    // Rename "require" to "localRequire" to avoid interference with r.js
    applyPatch(/\brequire\b/g, 'localRequire');
    applyPatch(/\bdefine\b/g, 'localDefine');
    // Rename module / exports to prevent the environment from affecting the
    // JsSIP runtime. E.g. webpack serializes all code and runs eval on the
    // resulting string after defining a CommonJS environment.
    // phone.js expects JsSIP to be a global object, but JsSIP will only export
    // itself to the global namespace of it is not a CommonJS module.
    applyPatch(/\bmodule\b/g, 'localJsSIPModule');
    applyPatch(/\bexports\b/g, 'localJsSIPExports');
    applyPatch(/[ \t]*debugerror.log = console.warn.bind\(console\);/g, ';');
    // global log spam: JsSIP
    applyPatch(/debug\('version %s', pkg\.version\);/, '');
    return content;

    // Equivalent to content = content.replace(searchTerm, replacement);
    // with error checking.
    function applyPatch(searchTerm, replacement) {
        var content2 = content.replace(searchTerm, replacement);
        if (content === content2) {
            console.error('WARNING WARNING WARNING');
            console.error('Patch NOT applied!');
            console.error('Searched for: ' + searchTerm);
            console.error('Replacement : ' + replacement);
            console.error('Check whether patchJsSIP in build.js is still up-to-date!');
        }
        content = content2;
    }
}
