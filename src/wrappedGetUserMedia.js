define('wrappedGetUserMedia', function() {
    'use strict';
    /**
     * This module wraps getUserMedia such that permission denied requests are
     * handled in a userfriendly way: A hint on enabling getUserMedia is shown.
     * This is only supported in Chrome, in all other browsers the exported
     * getUserMedia method is an alias for navigator.getUserMedia.
     *
     * When the page-covering permission request dialog is shown/hidden, the
     * bubbling cyMediaCaptureVisibilityChange event is dispatched. event.data
     * is a boolean that tells whether the dialog is shown or hidden.
     */
    var getUserMediaNative = navigator.mediaDevices &&
        navigator.mediaDevices.getUserMedia;
    if (getUserMediaNative)
        getUserMediaNative = getUserMediaNative.bind(navigator.mediaDevices);
    else
        return;

    var requestIdCounter = 0;
    var lastRequestId = 0;

    /**
     * getUserMedia with retry on failure.
     */
    async function getUserMediaWrapped(constraints) {
        var requestId = ++requestIdCounter;
        lastRequestId = requestId;
        var delayedPermissionReminder = setTimeout(showMiniGuide, 3000);
        try {
            return getUserMediaWrapped_(constraints, requestId);
        } finally {
            clearTimeout(delayedPermissionReminder);
        }
    }
    async function getUserMediaWrapped_(constraints, requestId) {
        var beforerequesttime = Date.now();
        try {
            return getUserMediaNative(constraints);
        } catch (error) {
            if (error.name !== 'PermissionDeniedError') {
                throw error;
            }
            if (Date.now() - beforerequesttime > 100) {
                // When the permission request is not immediately processed,
                // assume that the permission request was not automatically
                // denied, but denied as a response to a user action.
                hideGuide();
                throw error;
            }
            if (lastRequestId !== requestId) {
                // Allow only one recursive getUserMedia call, to avoid creating
                // zillions of microphone access requests after the user grants
                // the microphone permission.
                throw error;
            }
            // Automatically cancelled.
            // Explain how to allow access to the microphone.
            showGuide();
            // Keep polling until the user has responded to the permission
            // request.
            await new Promise(resolve => setTimeout(resolve, 100));
            return getUserMediaWrapped_(constraints, requestId);
        }
    }

    var guide;
    // TODO: Put Rex here?
    var APPNAME = 'This application';
    var labels = {
        please_approve: '[app] requires access to your microphone. Click on ' +
            'the "Allow" button at the top of the page to enable the ' +
            'microphone.',
        intro: '[app] requires access to your microphone. Please ' +
            'enable the microphone as follows:',
        step1: 'Click on [icon] button at the right of the location bar of ' +
            'the browser.',
        step2: 'Select "Ask if [host] wants to access your microphone".',
        step3: 'Click on the "Done" button.',
        step4: 'When the "[origin] wants to use your microphone" bar appears,' +
           ' click on "Allow". Reloading the page is not needed.',
        step5: 'Click here if you don\'t want to activate the microphone.',
    };
    if (/nl/i.test(navigator.language)) {
        APPNAME = 'Deze applicatie';
        labels.please_approve = '[app] vereist toegang tot je microfoon. Druk' +
            ' op de "Toestaan" knop boven de pagina om de microfoon in te ' +
            'schakelen.';
        labels.intro =
            '[app] vereist toegang tot je microfoon. Schakel deze microfoon ' +
            'als volgt in:';
        labels.step1 =
            'Druk op de [icon] knop aan de rechterkant van de adresbalk van ' +
            'je browser.';
        labels.step2 =
            'Kies "Vragen of [host] toegang wil tot je microfoon".';
        labels.step3 =
            'Druk op "Klaar".';
        labels.step4 =
            'Wanneer de "[origin] wil gebruikmaken van je microfoon" balk ' +
            'verschijnt, druk op "Toestaan".';
        labels.step5 =
            'Klik hier als je de microfoon niet wilt activeren.';
    }

    function showMiniGuide() {
        if (guide && document.contains(guide)) {
            // Don't show the simple instructions if the simple or the
            // extended instructions are already visible.
            return;
        }
        if (!guide) guide = createDialogContainer();
        guide.className = 'webrtc-chrome-guide-simple';
        guide.firstChild.innerHTML =
            labels.please_approve.replace('[app]', APPNAME);
        (document.body || document.documentElement).appendChild(guide);
        dispatchGuideVisibilityChange(true);
    }

    function createDialogContainer() {
        var overlay = document.createElement('div');
        overlay.style.cssText =
            'position:fixed;top:0;box-sizing:border-box;left:0;width:100%;' +
            'height:100%;line-height:100vh;text-align:center;padding:50px;' +
            'background:rgba(0,0,0,0.2);z-index:2000000;';
        overlay.innerHTML = '<div class="webrtc-chrome-guide" style="' +
            'display:inline-block;line-height:1.4em;font-size:1em;' +
            'text-align:left;padding:20px;background:white;color:black;' +
            '"></div>';
        return overlay;
    }

    function showGuide() {
        if (!guide) {
            guide = createDialogContainer();
        } else if (document.contains(guide) ||
                guide.classList.contains('webrtc-chrome-guide-extended')) {
            return;
        }
        // Icon of the media access denied button in the omnibox.
        var icon = '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA' +
            'BkAAAATCAYAAABlcqYFAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAPEgAADxIBI' +
            'ZvyMwAAAWBJREFUOI29kstqwlAURfMJfpVOfPycfkLHVSMZCe3AQaHgqNBRcFiqY' +
            'lBvTMzrdO/QK4mJlWtbhc3xnMNdKy8ryzL571h3lwwGg5tSx/hRYnqVxTP6/1WJU' +
            'soo5+Di7E8lRUGtJE1T0eFyv98bhWd4tm6mYyw5HA4SRVFe2WtBcf4rCQFHxxHV7' +
            'Urq+ycRK3vV6+V727Zvl/BKPQg+LUu8ZlMSgB1AWdnn805HXNctS5IkEZ1+vy+73' +
            'e5ifMBiyD4AXADIGq3XpZ778XgsRa6R5CTCF7QA8A3g90Yjr4tvAffklCRxHIsOl' +
            '9vt9mr4mYarlbxC8AIBa7hcymQyyffkFLkVybXwpR5xxfNWS6YQPEPAOsedcE5RR' +
            'cKXaZIQj2wGwSPAM4B93AGr7rk/P2MseWq35QHAKYABHg1nAcDsOee+IgmCQEzij' +
            'kbiAKQ2m9JceV4+d4fDyhlL7vC7i+QLbLi7spz5THQAAAAASUVORK5CYII=" ' +
            'style="width:25px;height:19px;vertical-align:middle;">';
        var host = location.host;
        var origin = location.origin + '/';

        guide.className = 'webrtc-chrome-guide-extended';
        guide.firstChild.innerHTML =
            labels.intro.replace('[app]', APPNAME) + '<br>' +
            '<ol style="margin:1em 0 1em 2em">' +
            '<li>' + labels.step1.replace('[icon]', icon) + '</li>' +
            '<li>' + labels.step2.replace('[host]', host) + '</li>' +
            '<li>' + labels.step3 + '</li>' +
            '<li>' + labels.step4.replace('[origin]', origin) + '</li>' +
            '<li><a href="javascript:// Deny">' + labels.step5 + '</a></li>' +
            '</ol>';
        guide.querySelector('a[href*="Deny"]').onclick = function(e) {
            e.preventDefault();
            e.stopPropagation();
            // Set lastRequestId to 0 to signal that the pending getUserMedia
            // call must fail and invoke the failure callback asap.
            lastRequestId = 0;
            hideGuide();
        };
        (document.body || document.documentElement).appendChild(guide);
        dispatchGuideVisibilityChange(true);
    }

    function hideGuide() {
        if (guide) {
            guide.style.display = 'none';
            dispatchGuideVisibilityChange(false);
            guide.remove();
            guide = null;
        }
    }

    function dispatchGuideVisibilityChange(isVisible) {
        // Dispatch a custom event to let the host know that the guide is
        // shown/hidden. Useful if the host needs to update the layout.
        guide.dispatchEvent(new CustomEvent('cyMediaCaptureVisibilityChange', {
            cancelable: true,
            bubbles: true,
            // event.detail = whether the guide is visible.
            detail: isVisible
        }));
    }

    return window.chrome ? getUserMediaWrapped : getUserMediaNative;
});
