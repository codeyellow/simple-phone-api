/* globals MediaSource */
/* exported setPhoneMock, unsetPhoneMock, sendIncomingForTesting */
/**
 * Usage:
 * var phone = ... instance of the Phone object from the simple-phone-api ...
 * setPhoneMock(phone)
 * // From now on, all outgoing calls are fake, i.e. no network activity.
 * unsetPhoneMock(phone)
 * // Original behavior is restored.
 *
 * // setPhoneMock can take options. By default a popup is shown to control the
 * // fake call. This popup can be hidden as follows:
 * setPhoneMock(phone, {interactive: false});
 * // To show the popup again:
 * unsetPhoneMock(phone);
 * setPhoneMock(phone);
 *
 * // Fake an incoming call
 * sendIncomingForTesting(phone, '06123456789');
 *
 * // Control a mocked call without popup:
 * unsetPhoneMock(phone);
 * setPhoneMock(phone, {interactive: false});
 * var phoneCall = phone.startCall('*1337');
 * FakeRTCSession.getControllerFor(phoneCall).answer();
 * FakeRTCSession.getControllerFor(phoneCall).terminate();
 */
'use strict';


/**
 * @param {Phone} phone
 * @param {Object=} testOptions
 * @param {boolean} testOptions.interactive - Enable popup (true by default).
 */
function setPhoneMock(phone, testOptions) {
    console.assert(phone.jssipPhone, 'Must be a Phone object'); // Duck-typed
    if (phone.jssipPhone._mockCallOriginal) {
        console.log('Not adding mock to phone, because mock already exists.');
        return;
    }
    // Do not use or rely on this variable outside this file.
    phone._phone_mock_options = testOptions || {};
    phone.jssipPhone._mockCallOriginal = phone.jssipPhone.call;
    phone.jssipPhone.call = function(callerId, callOptions) {
        var ua = this;
        var fakeRTCSessionOptions = {
            caller_id: callerId,
            mock_options: phone._phone_mock_options
        };
        // Up until simple-phone-api 0.2.17, the return value of .call had to
        // be an instance of JsSIP.RTCSession (exported as JsSIP.cyRTCSession).
        var cyRTCSession = window.JsSIP && window.JsSIP.cyRTCSession;
        if (cyRTCSession) {
            var rtcSession = Object.create(cyRTCSession.prototype);
            // We do not depend on inheritance (instanceof) in the mock, but
            // the mocked methods should be present.
            var proto = FakeRTCSession.prototype;
            Object.keys(proto).forEach(function(k) {
                if (typeof proto[k] == 'function') rtcSession[k] = proto[k];
            });
            FakeRTCSession.call(rtcSession, ua, fakeRTCSessionOptions);
            return rtcSession;
        }
        return new FakeRTCSession(ua, fakeRTCSessionOptions);
    };
    console.log('Phone.startCall is now mocked. Call unsetPhoneMock to undo.');
}

function unsetPhoneMock(phone) {
    console.assert(phone.jssipPhone, 'Must be a Phone object'); // Duck-typed
    if (phone.jssipPhone._mockCallOriginal) {
        phone.jssipPhone.call = phone.jssipPhone._mockCallOriginal;
        delete phone.jssipPhone._mockCallOriginal;
        delete phone._phone_mock_options;
    }
}

function sendIncomingForTesting(phone, remoteCallerId) {
    console.assert(phone.jssipPhone, 'Must be a Phone object'); // Duck-typed
    console.assert(typeof remoteCallerId === 'string',
            'Must be a string: ' + remoteCallerId);
    if (!phone._phone_mock_options) {
        console.error('Cannot call sendIncomingForTesting on the phone, ' +
                'because setPhoneMock was not called for it');
        return;
    }
    var rtcSession = new FakeRTCSession(phone.jssipPhone.ua, {
        caller_id: remoteCallerId,
        incoming: true,
        mock_options: phone._phone_mock_options
    });

    // MINIMAL mock of JsSIP.IncomingRequest
    var request = rtcSession._generateMessage();

    rtcSession.direction = 'incoming';
    // Triggers created + incoming request from Phone constructor.
    phone.jssipPhone.emit('newRTCSession', {
        session: rtcSession,
        request: request
    });
}


// MINIMAL mock of RTCSession, and ONLY the parts related to OUTGOING calls.
// Only parts that are currently used by phone.js are mocked, following YAGNI.

function FakeRTCSession(ua, fakeRTCSessionOptions) {
    // A "real" JsSIP.UA instance (jssipPhone).
    this.ua = ua;

    this.remote_identity = {
        // Used in transferCall (but the exact value does not matter because we
        // mock the transfer).
        uri: {
            // Used for incoming calls in newRTCSession event.
            user: fakeRTCSessionOptions.caller_id
        }
    };

    // EventEmitter implementation of JsSIP
    this._events = {};

    // For debugging purposes only.
    this._callerId = fakeRTCSessionOptions.caller_id;

    this._is_incoming = fakeRTCSessionOptions.incoming;
    this._is_accepted = false;
    this._is_terminated = false;
    this._dtmfs = [];
    FakeRTCSession.lastId =
        FakeRTCSession.lastId ? FakeRTCSession.lastId + 1 : 1;
    this._identifier_for_testing = FakeRTCSession.lastId;

    this._mockController = this._createController();

    if (fakeRTCSessionOptions.mock_options.interactive !== false)
        this._openControllerForOtherEnd();
    setTimeout(function() {
        // Automatically acknowledge call after 500ms
        this._mockController.outgoing_progress();
    }.bind(this), 500);
}

FakeRTCSession.getControllerFor = function(phoneCall) {
    // Duck-typing phone call
    console.assert('rtcSession' in phoneCall,
            'FakeRTCSession.getControllerFor\'s argument must be a PhoneCall!');
    console.assert(!phoneCall.terminated,
            'FakeRTCSession.getControllerFor requires a non-terminated call!');
    console.assert(phoneCall.call_id,
            'FakeRTCSession.getControllerFor requires that the "initialized" ' +
            'event of a PhoneCall has been fired!');
    // This is only set after the "initialized" event.
    return phoneCall.rtcSession._mockController;

};

// EventEmitter (partial, i.e. no once/off methods)
FakeRTCSession.prototype.on = function(type, callback) {
    if (!this._events[type]) this._events[type] = [];
    if (typeof callback != 'function')
        throw new Error('listener must be a function');
    this._events[type].push(callback);
};

FakeRTCSession.prototype.emit = function(type) {
    if (!this._events[type]) return;
    var args = [].slice.call(arguments, 1);
    var listeners = this._events[type].slice();
    for (var i = 0; i < listeners.length; ++i)
        listeners[i].apply(this, args);
};

FakeRTCSession.prototype.answer = function() {
    console.assert(!this._is_accepted, 'FakeRTCSession.answer already called!');
    // TODO: gUM using navigator.mediaDevices.getUserMedia
    setTimeout(function() {
        if (this._is_terminated) return;
        this._is_accepted = true;
        this.emit('accepted', {
            response: this._generateMessage()
        });
    }.bind(this), 2000);
};

FakeRTCSession.prototype.terminate = function() {
    if (this._is_terminated)
        throw new Error('terminate called while call was terminated!');
    this._is_terminated = true;
    if (!this._is_accepted) {
        this.emit('failed', {
            cause: 'Canceled', // === JsSIP.C.causes.CANCELED
            message: this._generateMessage()
        });
        return;
    }
    setTimeout(function() {
        this.emit('ended', {
            message: this._generateMessage()
        });
    }.bind(this), 200);
};

FakeRTCSession.prototype.hold = function() {
    if (this._is_terminated) return;
    if (!this._local_hold) {
        this._local_hold = true;
        this.emit('hold');
    }
    return true;
};

FakeRTCSession.prototype.unhold = function() {
    if (this._is_terminated) return;
    if (this._local_hold) {
        this._local_hold = false;
        this.emit('unhold');
    }
    return true;
};

FakeRTCSession.prototype.isOnHold = function() {
    return {
        local: this._local_hold
    };
};

FakeRTCSession.prototype.sendDTMF = function(dtmf) {
    if (this._is_terminated) return;
    var has_no_dtmf = this._dtmfs.length === 0;
    this._dtmfs.push.apply(this._dtmfs, /[0-9A-D*#]/g.exec(dtmf.toUpperCase()));
    if (has_no_dtmf && this._dtmfs.length) {
        var triggerDTMF = function() {
            if (this._is_terminated) return;
            var dtmf = this._dtmfs.shift();
            this.emit('newDTMF', {
                request: this._generateMessage(),
                dtmf: {
                    tone: dtmf
                },
                originator: 'local'
            });
            if (this._dtmfs.length) {
                setTimeout(triggerDTMF, 200);
            }
        }.bind(this);
        setTimeout(triggerDTMF, 200);
    }
};

FakeRTCSession.prototype.refer = function(target, options) {
    if (this._is_terminated) return;
    options = options || {};
    if (options.replaces && !(options.replaces instanceof FakeRTCSession)) {
        // Should not happen.
        throw new Error('Cannot use refer to replace a non-RTCSession!');
    }
    console.warn('refer: Mocked call transfer ' +
            (options.replaces ? '(warm)' : '(cold)'));
    setTimeout(function() {
        // From the perspective of the caller, the call hangs up after a
        // transfer. So simulate that by terminating the call from the other
        // end.
        this._mockController.terminate();
        if (options.replaces) {
            options.replaces._mockController.terminate();
        }
    }.bind(this), 500);
    return true;
};

// TODO: Remove. This is no longer used after simple-phone-api 0.2.26.
FakeRTCSession.prototype.sendRequest = function(method, options) {
    if (this._is_terminated) return;
    if (method !== 'REFER') {
        // sendRequest should no longer be used after simple-phone-api 0.2.26,
        // when used with JsSIP 0.7.x.
        throw new Error('rtcSession.sendRequest is not defined.');
    }
    // console.error gives a nice stack trace.
    console.error('NOT IMPLEMENTED: RTCSession::sendRequest', method, options);
};

// TODO: Remove. This is no longer used after simple-phone-api 0.2.26.
FakeRTCSession.prototype.getLocalStreams = function() {
    // Maybe return an empty array when listen-in is detected?
    return [ new MediaSource() ];
};

// TODO: Remove. This is no longer used after simple-phone-api 0.2.26
FakeRTCSession.prototype.getRemoteStreams = function() {
    return [ new MediaSource() ];
};

// This is a minimal mock of RTCPeerConnection as used by simple-phone-api.
FakeRTCSession.prototype.connection = {
    getRemoteStreams: function() {
        return [ new MediaSource() ];
    },
};

FakeRTCSession.prototype._generateMessage = function() {
    var suffix = '-' + this._identifier_for_testing;
    return {
        getHeader: function(name) {
            if (name === 'X-CY-Call-ID')
                return 'MOCK-X-CY-Call-ID' + suffix;
            else if (name === 'X-CY-Leg-UUID')
                return 'MOCK-X-CY-Leg-UUID' + suffix;
            else if (name === 'X-CY-Other-Leg-UUID')
                return 'MOCK-X-CY-Other-Leg-UUID' + suffix;
        }
    };
};

// Create a controller to simulate behavior from the remote side of this fake
// RTCSession. It is safe to use the methods without changing "this".
FakeRTCSession.prototype._createController = function() {
    var rtcSession = this;

    return {
        answer: answer,
        outgoing_progress: outgoing_progress,
        terminate: terminate,
    };

    function answer() {
        if (rtcSession._is_accepted) return;
        if (rtcSession._is_terminated) return;
        rtcSession.emit('accepted', {
            response: rtcSession._generateMessage()
        });
    }

    function outgoing_progress() {
        // We don't check whether this method was already called before, because
        // it is technically possible to receive multiple progress events.
        if (rtcSession._is_accepted) return;
        if (rtcSession._is_terminated) return;
        rtcSession.emit('progress', {
            originator: rtcSession._is_incoming ? 'local' : 'remote',
            response: rtcSession._generateMessage()
        });
    }

    function terminate() {
        if (rtcSession._is_terminated) return;
        rtcSession._is_terminated = true;

        if (rtcSession._is_accepted) {
            rtcSession.emit('ended', {
                message: rtcSession._generateMessage()
            });
        } else {
            rtcSession.emit('failed', {
                cause: 'Terminated', // === JsSIP_C.causes.BYE
                message: rtcSession._generateMessage()
            });
        }
    }
};

// Open a popup window which allows the tester to act as the other leg.
// (e.g. accept the call, or hangup the call).
FakeRTCSession.prototype._openControllerForOtherEnd = function() {
    // Assume that opening the popup succeeds
    var w = window.open('', '_blank', 'popup,width=400,height=200,' +
            'top=100,left=' + (screen.availWidth - 400) / 2);
    if (!w) {
        console.error('Failed to FakeRTCSession popup for ' + this._callerId);
        return;
    }
    var closeController = function() {
        if (!w.closed) w.close();
        window.removeEventListener('unload', closeController);
    };
    // Close popup when initiator is closed / reload
    window.addEventListener('unload', closeController);
    // Remove unload listener when popup closes.
    w.addEventListener('unload', closeController);
    this.on('ended', closeController);
    this.on('failed', closeController);

    var doc = w.document;
    doc.title = 'Fake call to: ' + this._callerId;
    doc.body.textContent = 'Fake call to ' + this._callerId;
    doc.body.appendChild(doc.createElement('br'));
    var acceptButton = doc.createElement('button');
    acceptButton.textContent = 'Answer call';
    var hangupButton = doc.createElement('button');
    hangupButton.textContent = 'End call';
    doc.body.appendChild(acceptButton);
    doc.body.appendChild(hangupButton);
    acceptButton.onclick = function() {
        this._mockController.answer();
    }.bind(this);
    hangupButton.onclick = function() {
        closeController();
        this._mockController.terminate();
    }.bind(this);

    this.on('accepted', function() {
        acceptButton.disabled = true;
    });
};
