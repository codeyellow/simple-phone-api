/* globals JsSIP, AudioContext */
/* globals MediaStream */
define('phone', [
    'backbone',
    'underscore',
    'phone_sounds',
    'wrappedGetUserMedia'
], function(Backbone, _, PhoneSounds, wrappedGetUserMedia) {
    'use strict';
    /**
     * @module phone
     * Exports a Phone class that can be used to receive and initiate calls.
     * Each phone call is associated with a PhoneCall object.
     *
     * ## Typical method calls and event sequences.
     *
     * Initializing a phone, connection failed.
     * 1. new Phone(options)
     * 2. Phone#disconnected
     *
     * Initializing a phone, connection succeeded, failed and reconnected.
     * 1. new Phone(options)
     * 2. Phone#connected
     * 3. Phone#disconnected
     * 4. Phone#connected
     *
     * Outgoing call
     *
     * 1. Phone.startCall()
     * 2. Phone#created
     * 3. PhoneCall#initialized
     * 4. PhoneCall#outgoing_progress
     * 5. PhoneCall#answered
     * 6. PhoneCall#ended
     *
     * Outgoing call, failure (e.g. when the user is not authenticated)
     *
     * 1. Phone.startCall()
     * 2. Phone#created
     * 3. PhoneCall#ended
     *
     * Incoming call, accept
     *
     * 1. Phone#created
     * 2. Phone#incoming
     * 3. PhoneCall.answer()
     * 4. PhoneCall#initialized
     * 5. PhoneCall#answered
     * 6. PhoneCall#ended
     *
     * Incoming call, reject
     *
     * 1. Phone#created
     * 2. Phone#incoming
     * 3. PhoneCall.terminate()
     * 4. PhoneCall#ended
     *
     * Incoming call (attended transfer from the other end), end call.
     *
     * 1. Phone#created
     * 2. Phone#incoming
     * 3. PhoneCall.answer()
     * 4. PhoneCall#initialized
     * 5. PhoneCall#answered
     * 6. PhoneCall#call_id_changed
     * 7. PhoneCall.terminate()
     * 8. PhoneCall#ended
     *
     * Turn off phone
     *
     * 1. Phone.destroy()
     * 2. PhoneCall#ended for each existing phone call
     * 3. Phone#disconnected
     * 4. Phone#destroyed
     *
     * The last events can be triggered in any sequence.
     *
     * At any time during an active call, the PhoneCall#dtmf or
     * PhoneCall#holdchanged events can be triggered.
     *
     * As you can see, each call starts with a Phone#created event and ends with
     * a PhoneCall#ended event regardless of whether the call succeeded.
     * Look at the following event type declarations to learn more about what is
     * available at each event.
     *
     */
    var exports = {};

    /**
     * A valid phone number, canonically represented as:
     * - External numbers are in E164 format.
     * - Internal numbers are prefixed with a '*'.
     * TODO: Create custom class to handle this conversion/validation logic?
     * @typedef {number|string} PhoneNumber
     */

    /**
     * @typedef {object} PhoneCallEvent
     * @property {string} type - Name of event.
     * @property {PhoneCall} phoneCall - The phoneCall that is associated with
     * the PhoneCall event.
     */

    /**
     * Event that is fired when a Phone is destroyed.
     *
     * @event Phone#destroyed
     * @type  {object}
     * @property {string} type - Name of event ("destroyed").
     * @property {Phone} phone - The Phone object that is being destructed.
     */

    /**
     * Event that is fired when a new PhoneCall object has been constructed.
     *
     * @event Phone#created
     * @type {PhoneCallEvent}
     */

    /**
     * Event that is fired when a new call comes in.
     *
     * @event Phone#incoming
     * @type {PhoneCallEvent}
     * @property {string} remote_caller_id - Phone number of the other party.
     * @property {IncomingRequest} request - Request associated with the
     *   incoming call.
     */

    /**
     * Event that is fired when the phone is connected to the SIP server.
     *
     * @event Phone#connected
     * @type {object}
     */

    /**
     * Event that is fired when the phone is disconnected from the SIP server.
     * Note that the phone automatically attempts to reconnect to the server
     * upon disconnect.
     *
     * @event Phone#disconnected
     * @type {object}
     */

    /**
     * Event that is fired when the call info of the incoming or outgoing call
     * has been initialized. This means that the VOIP server has supplied the
     * Call-ID associated with the call.
     *
     * @event PhoneCall#initialized
     * @type {PhoneCallEvent}
     * @property {IncomingRequest} request - Request associated with the call.
     */

    /**
     * Event that is fired when an outgoing call has been acknowledged by the
     * server.
     *
     * @event PhoneCall#outgoing_progress
     * @type {PhoneCallEvent}
     */

    /**
     * Event that is fired when the incoming or outgoing call has been answered.
     *
     * @event PhoneCall#answered
     * @type {PhoneCallEvent}
     */

    /**
     * Event that is fired when the call has ended or failed.
     *
     * @event PhoneCall#ended
     * @type {PhoneCallEvent}
     * @property {string=} error - Set to an error message if the call ended due
     *   to a failure.
     * @property {phone.ERROR=} errorCode - Set if the error has a publicly
     *   defined meaning.
     */

    /**
     * Event that is fired when a DTMF is received or sent.
     *
     * @event PhoneCall#dtmf
     * @type {PhoneCallEvent}
     * @property {string} dtmf - String indicating the DTMF symbol.
     * @property {boolean} isRemote - Whether the DTMF was received from the
     *   other party.
     */

    /**
     * Event that is fired when the hold state of the phone call changes.
     * Use event.phoneCall.isOnHold() to get the actual hold state.
     *
     * @event PhoneCall#holdchanged
     * @type {PhoneCallEvent}
     */

    /**
     * Event that is fired when the call_id of a PhoneCall changes. The new ID
     * is stored in this.call_id, use event.previous_call_id to get the old ID.
     *
     * @event Phone#call_id_changed
     * @type {PhoneCallEvent}
     * @property {string} previous_call_id - The previous call_id.
     */

    /**
     * Dispatch a PhoneCallEvent on eventTarget.
     *
     * @private
     * @param {Backbone.Events} eventTarget - Target to dispatch the event to.
     * @param {string} eventName - The name of the event.
     * @param {PhoneCall=} phoneCall - PhoneCall instance associated with event.
     * @param {object=} extraData - Optional extra event data.
     */
    function emitEvent(eventTarget, eventName, phoneCall, extraData) {
        var event = extraData ? _.clone(extraData) : {};
        event.type = eventName;
        event.phoneCall = phoneCall;
        try {
            eventTarget.trigger(eventName, event);
        } catch (e) {
            console.error('Error in ' + eventName + ' event handler: ' + e +
                    '\n' + e.stack);
        }
    }

    /**
     * Create a new Phone instance that can be used to initiate and accept
     * calls.
     *
     * @module phone.Phone
     * @constructor
     * @param {object} options
     * @param {string} options.username - ID of the SIP extension.
     * @param {string} options.password - Password of SIP extension.
     * @param {string} options.sip_domain - Host name (or IP address) of a SIP
     * address.
     * @param {string[]} options.ws_servers - A non-empty list of WebSocket URLs
     * to the VOIP server. Example: wss://example.com:8088/ws.
     * @param {string=} options.display_name - Name as displayed to others.
     * @property {PhoneCall[]} phoneCalls - All active PhoneCalls associated
     *   with this Phone.
     * @property {boolean} connected - Whether the phone is connected to the
     *   signaling server. Initially this value is null, but as soon as the
     *   connection fails or succeeds, this will be set to true or false.
     * @fires Phone#created
     * @fires Phone#incoming
     */
    function Phone(options) {
        bindAllInstanceMethods(this);
        _.extend(this, Backbone.Events);

        assert(options, 'options are required');
        options = _.pick(options, 'username', 'password', 'sip_domain',
                    'ws_servers', 'display_name');
        assert(options.username, 'options.username is required');
        assert('password' in options, 'options.password is required');
        assert(options.sip_domain, 'options.sip_domain is required');
        assert(options.ws_servers instanceof Array,
                    'array options.ws_servers is required');
        assert(options.ws_servers.length,
                    'array options.ws_servers must be non-empty');

        // Require('phone').isSupported can be used to test whether WebRTC is
        // supported.
        assert(wrappedGetUserMedia, 'WebRTC must be supported by the browser.');

        this.connected = null;

        this.options = options;
        var config = {
            sockets: options.ws_servers.map(
                uri => new JsSIP.WebSocketInterface(uri)),
            uri: 'sip:' + options.username + '@' + options.sip_domain,
            password: options.password,
            display_name: options.display_name,
            // JsSIP added support for session timers in 0.6.1, with an expires
            // of 60 seconds. But our FS backend requires a minimum of 120.
            // We could either override the value, or disable timers. I have
            // chosen to do the latter since this library never used timers, and
            // that went well.
            session_timers: false,
        };
        // TLS is offloaded to nginx which speaks unencrypted SIP with the
        // Freeswitch backend. Since Freeswitch checks the transport type in the
        // Via header, JSSIP should set "WS" (and not "WSS"). Otherwise the
        // connection will be dropped.  For backwards compatibility, assume that
        // port 8088 is a direct connection with Freeswitch.
        if (options.ws_servers.some(function(url) {
            var parts = /^wss:\/\/[^:]+:(\d+)/.exec(url);
            // if scheme is 'ws', do not set hack.
            // if scheme is 'wss' and port is not '8088', assume TLS proxy.
            return parts && parts[1] !== '8088';
        })) {
            // set Via: SIP/2.0/WS instead of SIP/2.0/WSS
            config.hack_via_ws = true;
        }

        var phone = this;
        var jssipPhone = new JsSIP.UA(config);

        jssipPhone.on('connected', this.onPhoneConnected);
        jssipPhone.on('disconnected', this.onPhoneDisconnected);
        jssipPhone.on('newRTCSession', function onNewRTCSession(event) {
            var rtcSession = event.session;
            if (rtcSession.direction == 'incoming') {
                var phoneCall = new PhoneCall({ incoming: true });
                emitEvent(phone, 'created', phoneCall);
                var extraData = {
                    remote_caller_id: rtcSession.remote_identity.uri.user,
                    request: new IncomingRequest(event.request)
                };
                emitEvent(phone, 'incoming', phoneCall, extraData);
                phoneCall.setRTCSession(rtcSession, event.request);
            }
        });
        jssipPhone.start();
        this.jssipPhone = jssipPhone;

        // Manage the list of PhoneCalls associated with this Phone.
        var phoneCalls = this.phoneCalls = [];
        this.on('created', function(event) {
            var phoneCall = event.phoneCall;
            phoneCalls.push(phoneCall);
            phoneCall.once('ended', function() {
                var i = phoneCalls.indexOf(phoneCall);
                assert(i != -1, 'PhoneCall not found in "ended" event!');
                phoneCalls.splice(i, 1);
            });
        });

    }

    /**
     * Initiate a new call session.
     *
     * @param {PhoneNumber} destination - Number to call.
     * @param {object=} options - Call options.
     * @param {string[]} options.extraHeaders - Extra headers to add to the SIP
     *   request. E.g. `['X-CY-custom-header: value']`.
     * @param {boolean} [options.audio=true] - Whether to send audio.
     * @return {PhoneCall} The call session associated with this call.
     * @fires Phone#created
     */
    Phone.prototype.startCall = function(destination, options) {
        assert(this.jssipPhone, 'The phone must still exist');
        options = options || {};
        var phoneCall = new PhoneCall({ incoming:false });
        emitEvent(this, 'created', phoneCall);

        var callerId = 'sip:' + destination + '@' + this.options.sip_domain;
        var callOptions = {
            extraHeaders: options.extraHeaders,
            // Constraints for getUserMedia.
            mediaConstraints: {
                // audio enabled by default.
                audio: 'audio' in options ? !!options.audio : true,
                video: false
            },
            // Config passed to RTCPeerConnection constructor.
            pcConfig: getPeerConnectionConfig(),
            // SDP constraints, passed to PeerConnection's createOffer method.
            rtcOfferConstraints: {
                offerToReceiveAudio: true,
                offerToReceiveVideo: false
            },
            // Constraints passed to RTCPeerConnection constructor.
            rtcConstraints: {
                optional: [{
                    DtlsSrtpKeyAgreement: 'true'
                }]
            }
        };
        var phone = this;
        // Note: setTimeout and getUserMedia are asynchronous. This allows
        // callers of startCall to bind events to the returned PhoneCall object.
        if (callOptions.mediaConstraints.audio) {
            phoneCall.getUserMedia(callOptions.mediaConstraints)
                .then(function(stream) {
                    callOptions.mediaStream = stream;
                    doStartCall(true);
                }, function(error) {
                    // Note: callOptions.mediaStream is not set.
                    doStartCall(true);
                });
        } else {
            setTimeout(function() {
                // If listening in recvonly mode, we don't have to get a media
                // stream from the user.
                doStartCall(false);
            });
        }

        async function doStartCall(expectStream) {
            if (!phone.jssipPhone) {
                // phone.destroy() was called.
                // TODO: Detect that doStartCall was not called yet, and emit
                // the 'ended' event in destroy instead of here, because at that
                // point we already know that the phoneCall will be ended? This
                // allows listeners to receive the 'ended' event before the user
                // approves / denies a user media request.
                emitEvent(phoneCall, 'ended', phoneCall);
                return;
            }
            if (phoneCall.terminated) {
                // phoneCall.terminate() was called.
                emitEvent(phoneCall, 'ended', phoneCall);
                return;
            }
            if (expectStream && !callOptions.mediaStream) {
                // Audio requested but not received.
                emitEvent(phoneCall, 'ended', phoneCall, {
                    error: exports.ERROR.USER_DENIED_MEDIA_ACCESS
                });
                return;
            }

            if (phone.jssipPhone._transport.isConnecting()) {
                // If startCall is called while a connection to the SIP server
                // is being opened, wait until the connection attempt has
                // finished (succeeded or failed) before sending a call request.
                await new Promise(resolve => {
                    phone.jssipPhone.once('connected', resolve);
                    phone.jssipPhone.once('disconnected', resolve);
                });
                if (!phone.jssipPhone || phoneCall.terminated) {
                    // Phone was destroyed or terminate() was called between
                    // the call start and the established connection.
                    emitEvent(phoneCall, 'ended', phoneCall);
                    return;
                }
            }

            var rtcSession;
            try {
                rtcSession = phone.jssipPhone.call(callerId, callOptions);
            } catch (e) {
                var extraData = {};
                extraData.error = 'runtime error in jssipPhone.call: ' + e;
                emitEvent(phoneCall, 'ended', phoneCall, extraData);
                throw e;
            }
            phoneCall.setRTCSession(rtcSession);
        }

        return phoneCall;
    };

    /**
     * Listen in to the last call of a participant.
     *
     * @param {PhoneNumber} number - Phonenumber of a participant of a call.
     */
    Phone.prototype.listenInCall = function(number) {
        // TODO: Support listen-in-by-uuid? The backend already supports this
        // via listenin_number/<uuid>
        var options = {
            audio: false
        };
        return this.startCall('listenin_number/' + number, options);
    };

    /**
     * Destroy the phone - all calls will be dropped and the phone becomes
     * unusable.
     *
     * @fires Phone#destroyed
     */
    Phone.prototype.destroy = function() {
        var jssipPhone = this.jssipPhone;
        if (jssipPhone) {
            this.jssipPhone = null;
            window.removeEventListener('online', this.tryReconnect);
            clearTimeout(this._disconnectRetryTimer);
            jssipPhone.stop();
            emitEvent(this, 'destroyed', null, { phone: this });
        }
    };

    /**
     * Try to reconnect to the server on failure.
     *
     * @private
     */
    Phone.prototype.tryReconnect = function() {
        if (navigator.onLine) {
            window.removeEventListener('online', this.tryReconnect);
        }
        if (this.jssipPhone && !this.jssipPhone.isConnected()) {
            this.logDebug('info', 'Trying to reconnect after a disconnect');
            this.jssipPhone.start();
        }
    };

    /**
     * Print a message to the console.
     *
     * @private
     * @param {string} [level=log] - Log level (log, info, debug, warn, error).
     */
    Phone.prototype.logDebug = function(level, category, label, content) {
        // Always show errors
        if (exports.debugEnabled || level == 'error') {
            var consoleMethod = console[level] || console.log;
            if (label === undefined) label = '';
            if (arguments.length < 4 && content === undefined) content = '';
            consoleMethod.call(console, category, label, content);
        }
    };

    /**
     * Triggered when the WebSocket connection fails.
     *
     * @private
     */
    Phone.prototype.onPhoneDisconnected = function(event) {
        // Try to reconnect as soon as possible.
        // This method aggressively attempts to reconnect to the server,
        // because the phone must always be connected, by design.
        if (this.jssipPhone) {
            clearTimeout(this._disconnectRetryTimer);
            this._disconnectRetryTimer = setTimeout(function() {
                if (navigator.onLine) {
                    // If the internet is still up, just try to reconnect.
                    // If the connection fails, then onPhoneDisconnected will be
                    // called again, until the connection succeeds (or until
                    // .destroy() is called).
                    this.tryReconnect();
                } else {
                    // If the internet is down, reconnect when it gets back up.
                    window.addEventListener('online', this.tryReconnect);
                }
            }.bind(this), 1000);
        }
        // Fire a disconnected event if this.connected === true|null
        if (this.connected !== false) {
            this.connected = false;
            emitEvent(this, 'disconnected', null);
        }
    };

    /**
     * Triggered when the WebSocket connection has been set up.
     *
     * @private
     */
    Phone.prototype.onPhoneConnected = function(event) {
        // Fire connected event iff this.connected === false|null
        if (this.connected !== true) {
            this.connected = true;
            emitEvent(this, 'connected', null);
        }
    };

    /**
     * Manage a call session.
     *
     * @module phone.PhoneCall
     * @constructor
     * @param {object=} options
     * @param {boolean} options.incoming - Whether the call is an incoming call.
     */
    function PhoneCall(options) {
        bindAllInstanceMethods(this);
        _.extend(this, Backbone.Events);

        options = options || {};

        /**
         * The RTC session associated with this PhoneCall instance.
         *
         * @private
         * @type {?JsSIP.RTCSession}
         */
        this.rtcSession = null;

        /**
         * The Call-ID of the phone call. This property is only available after
         * the PhoneCall#initialized event has fired.
         *
         * @type {string}
         */
        this.call_id = null;

        /**
         * The UUID of this phone's call leg.
         * Only available after PhoneCall#initialized has fired.
         *
         * @type {string}
         */
        this.my_leg_uuid = null;

        /**
         * The UUID of the other leg of the call.
         * TODO: This property does not automatically update if the current call
         * is transferred by the other party! It does not make much sense in a
         * conference call either (which is not implemented yet)!
         * Only available after PhoneCall#initialized has fired.
         *
         * @type {string}
         */
        this.other_leg_uuid = null;

        /**
         * Whether the call has been terminated.
         */
        this.terminated = false;

        /**
         * Whether the call was marked as an incoming call.
         */
        this.incoming = !!options.incoming;

        /**
         * Whether the phone call has been answered.
         */
        this.answered = false;
        this.once('answered', function() {
            this.answered = true;
        }.bind(this));

        /**
         * Whether .answer() was called.
         */
        this.answerRequested = false;

        /**
         * Whether the call has been put on hold. This property is only used for
         * maintaining the hold status until the RTCSession is ready.
         *
         * @private
         */
        this.holdStatus = false;

        /**
         * The hold state of rtcSession. Could be different from this.holdStatus
         * if this.rtcSession is not set, or if RTCSession rejected the hold
         * request.
         *
         * @private
         */
        this.holdStatusCommitted = false;

        /**
         * Audio object for playing incoming audio.
         *
         * @private
         * @type {HTMLAudioElement}
         */
        this.incomingAudio = document.createElement('audio');
        this.incomingAudio.autoplay = true;

        if (exports.audioContext) {
            /**
             * Gain node for manipulating the input volume (if supported).
             *
             * @private
             * @type {GainNode}
             */
            this.audioGainNode = exports.audioContext.createGain();
        }
    }

    /**
     * Answer an incoming call.
     */
    PhoneCall.prototype.answer = function() {
        this.answerRequested = true;
        if (this.rtcSession && !this.answered) {
            this.answered = true;
            this.getUserMedia({
                audio: true,
                video: false
            }).catch((error) => {
                console.error('getUserMedia failed ' + error, error);
                return null; // Handled further below.
            }).then((stream) => {
                if (this.terminated) {
                    // If this.rtcSession is set, then calling .terminate() will
                    // already destroy this.rtcSession which in turns triggers
                    // the 'ended' event.
                    // If the remote party terminates the call, then the 'ended'
                    // event is also triggered.
                    // In either case, we can safely ignore the callback of the
                    // getUserMedia request. Note that this.getUserMedia
                    // already stops the stream if the call was terminated.
                    return;
                }
                if (!stream) {
                    this.terminate();
                    emitEvent(this, 'ended', this, {
                        error: exports.ERROR.USER_DENIED_MEDIA_ACCESS
                    });
                    return;
                }
                this.rtcSession.answer({
                    pcConfig: getPeerConnectionConfig(),
                    mediaStream: stream
                });
            });
        }
    };

    /**
     * Terminate a call and invalidate the PhoneCall instance.
     */
    PhoneCall.prototype.terminate = function() {
        if (this.rtcSession) {
            this.rtcSession.terminate({
            });
            this.rtcSession = null;
        }
        this.terminated = true;
        this.incomingAudio = null;
        this.audioGainNode = null;
        // The outgoing audio stream will be canceled once 'ended' is triggered.
    };

    /**
     * Hold the call, i.e. stop exchanging audio with the other party.
     */
    PhoneCall.prototype.hold = function() {
        if (this.terminated) {
            return;
        }
        this.holdStatus = true;
        this.tryCommitHoldStatus();
    };

    /**
     * Unhold the call.
     */
    PhoneCall.prototype.unhold = function() {
        if (this.terminated) {
            return;
        }
        this.holdStatus = false;
        this.tryCommitHoldStatus();
    };

    /**
     * @return {boolean} Whether the current call has been put on hold.
     */
    PhoneCall.prototype.isOnHold = function() {
        if (this.rtcSession && !this.terminated &&
            this.isReallyOnHold() !== this.holdStatus) {
            console.warn('Actual hold state differs from cached hold state: ' +
                    this.isReallyOnHold() + ' vs ' + this.holdStatus);
        }
        return this.holdStatus;
    };

    /**
     * @return {boolean} Whether the (un)hold request was processed, and the
     *   call is really put on hold.
     */
    PhoneCall.prototype.isReallyOnHold = function() {
        if (this.terminated) {
            return this.holdStatus;
        }
        if (!this.rtcSession) {
            return this.holdStatusCommitted;
        }
        return this.rtcSession.isOnHold().local;
    };

    /**
     * Send DTMF tones to the other party.
     *
     * @param {string|number} keys - A sequence of valid DTMF symbols.
     */
    PhoneCall.prototype.sendKey = function(keys) {
        assert(this.rtcSession, 'Cannot send keys outside a call!');
        this.rtcSession.sendDTMF(keys);
    };

    /**
     * Transfer the call to a different number.
     * This is a fire-and-forget, there are no notifications about whether the
     * transfer succeeded.
     *
     * @param {PhoneNumber} destination - Destination number.
     * @param {PhoneCall=} toPhoneCall - If set, this call will be transferred
     *   to the given |PhoneCall|. This PhoneCall and |toPhoneCall| will be
     *   disconnected from this phone and then connected to each other. If this
     *   is set, then |destination| can be set to ''.
     */
    PhoneCall.prototype.transferCall = function(destination, toPhoneCall) {
        assert(this.rtcSession, 'Cannot transfer call without call!');
        assert(typeof destination == 'string', 'destination must be a string');
        assert(toPhoneCall === undefined || toPhoneCall instanceof PhoneCall,
               'toPhoneCall must not be set, or it should be a PhoneCall');
        // Attended transfer only makes sense if the receiver exists. So
        // require the call to not be terminated.
        assert(!toPhoneCall || !toPhoneCall.terminated,
               'toPhoneCall is set but the call was already terminated!');

        // If transferCall is immediately called after startCall(), then
        // the RTCSession is not ready yet. Wait until it is set.
        // This should usually not happen because in an attended transfer,
        // the caller first talks with the callee, but in case the caller
        // wants to transfer without talking, just transfer it.
        if (toPhoneCall && !toPhoneCall.rtcSession) {
            assert(!toPhoneCall.call_id,
                   'rtcSession not set implies that call_id not set, ' +
                   'but call_id is set!');
            var onInitialized = function() {
                removeListeners();
                assert(toPhoneCall.rtcSession, 'rtcSession should be set');
                this.transferCall(destination, toPhoneCall);
            }.bind(this);
            var removeListeners = function() {
                toPhoneCall.off('initialized', onInitialized);
                toPhoneCall.off('ended', removeListeners);
            };
            toPhoneCall.on('initialized', onInitialized);
            toPhoneCall.on('ended', removeListeners);
            return;
        }

        // TODO: Test whether destination exists before transferring?
        // For now, it is the caller's responsibility to make sure that
        // the input is okay.
        var extraHeaders = [
            'Contact: ' + this.rtcSession._ua.contact,
        ];
        if (!destination && toPhoneCall && toPhoneCall.rtcSession) {
            destination = toPhoneCall.rtcSession.remote_identity.uri;
        }
        var referred = this.rtcSession.refer(destination, {
            extraHeaders: extraHeaders,
            replaces: toPhoneCall && toPhoneCall.rtcSession,
            // TODO: use eventHandlers to monitor the transfer?
            // http://jssip.net/documentation/0.7.x/api/session/#method_refer
        });
        assert(referred !== false, 'Cannot transfer a call that is not yet ' +
                'accepted or that has already been transferred.');
    };

    /**
     * Invite another participant to the current call.
     *
     * @param {PhoneNumber} callee_id - Phone number of the callee.
     */
    PhoneCall.prototype.inviteParticipant = function(callee_id) {
        // TODO: Implement
    };

    /**
     * Set the (microphone) input volume as sent to the other party.
     *
     * @param {number} volume - number between 0 and 1, inclusive.
     */
    PhoneCall.prototype.setInputVolume = function(volume) {
        assert(volume >= 0 && volume <= 1, 'Volume must be in range [0, 1].');
        if (this.audioGainNode) {
            this.audioGainNode.gain.value = volume;
        } else if (volume != 1) {
            console.warn('Input volume not changed to ' + volume +
                    ' because the Web Audio API is not supported!');
        }
    };

    /**
     * Get the (microphone) input volume as sent to the other party.
     *
     * @return {number} Input volume level, a number between 0 and 1, inclusive.
     */
    PhoneCall.prototype.getInputVolume = function() {
        if (!this.audioGainNode) {
            // When the Web Audio API is not supported, the volume is always 1.
            return 1;
        }
        return this.audioGainNode.gain.value;
    };

    /**
     * Change the volume of the audio received from the other party.
     *
     * @param {number} volume - number between 0 and 1, inclusive.
     */
    PhoneCall.prototype.setVoiceVolume = function(volume) {
        this.incomingAudio.volume = volume;
    };

    /**
     * Get the volume of the audio received from the other party.
     *
     * @return {number} Volume level, a number between 0 and 1, inclusive.
     */
    PhoneCall.prototype.getVoiceVolume = function() {
        return this.incomingAudio.volume;
    };

    // Private methods
    /**
     * Associate a RTCSession with the PhoneCall.
     *
     * @private
     * @param {JsSIP.RTCSession} rtcSession
     * @param {JsSIP.IncomingRequest=} request
     */
    PhoneCall.prototype.setRTCSession = function(rtcSession, request) {
        var phoneCall = this;

        phoneCall.rtcSession = rtcSession;
        rtcSession.on('sdp', this.rtcSession_onsdp);
        rtcSession.on('icecandidate', this.rtcSession_onicecandidate);
        rtcSession.on('progress', this.rtcSession_onprogress);
        rtcSession.on('accepted', this.rtcSession_onaccepted);
        rtcSession.on('failed', this.rtcSession_onfailed);
        rtcSession.on('ended', this.rtcSession_onended);
        rtcSession.on('newDTMF', this.rtcSession_ondtmf);
        rtcSession.on('hold', this.rtcSession_onholdchanged);
        rtcSession.on('unhold', this.rtcSession_onholdchanged);
        rtcSession.on('cy_info', this.rtcSession_oncy_info);

        if (request) {
            /**
             * @fires PhoneCall#initialized
             */
            this.receiveSIPMessage(request);
        }
        if (!this.rtcSession) {
            // .terminate() was called during the "initialized" event.
            return;
        }
        if (this.terminated) {
            // .terminate() was called during the "incoming" event.
            this.terminate();
            return;
        }
        // Call .answer() again if it was called but not handled (this happens
        // when .answer() is called during the incoming event).
        if (this.incoming && this.answerRequested && !this.answered) {
            this.answer();
        }
        // Call .hold() if it was called but not handled.
        if (this.holdStatus && this.holdStatusCommitted !== this.holdStatus) {
            this.hold();
        }
    };

    /**
     * @private
     */
    PhoneCall.prototype.tryCommitHoldStatus = function() {
        clearTimeout(this._holdOrUnholdTimer);
        if (this.terminated) {
            return;
        }
        if (!this.rtcSession) {
            // If the call was not terminated and the RTCSession is not set,
            // then we are right before the call was created.
            // The hold state will be checked by setRTCSession, so return now.
            return;
        }
        if (this.holdStatus === this.holdStatusCommitted) {
            return;
        }
        var succeeded = false;
        if (this.holdStatus) {
           succeeded = this.rtcSession.hold();
        } else {
           succeeded = this.rtcSession.unhold();
        }
        // Guard against future incompatible updates by JsSIP.
        assert(typeof succeeded === 'boolean',
               'hold()/unhold() must return a boolean');
        if (succeeded) {
            this.holdStatusCommitted = this.holdStatus;
        } else {
            // Keep retrying.
            this._holdOrUnholdTimer = setTimeout(this.tryCommitHoldStatus, 100);
        }
    };

    /**
     * Get a MediaStream for the given constraints. The media stream is tied to
     * the lifetime of the PhoneCall instance: If the phone call has ended
     * before the user approves the media stream request, then the promise is
     * rejected.
     *
     * @private
     * @param {object} mediaConstraints - Object to be passed to getUserMedia.
     * @returns {Promise<MediaStream>}
     *   If the request succeeded, the first parameter will be a MediaStream.
     *   Otherwise the promise will be rejected.
     */
    PhoneCall.prototype.getUserMedia = async function(mediaConstraints) {
        if (this.terminated) {
            throw new Error('Skipping getUserMedia because call has ended');
        }

        let stream = await callGetUserMedia(mediaConstraints);
        if (this.terminated) {
            MediaStream_stopPolyfill.call(stream);
            throw new Error('Stopping getUserMedia because call has ended');
        }
        // Stop the media stream recording when the phone call ends.
        this.once('ended', MediaStream_stopPolyfill.bind(stream));

        if (mediaConstraints.audio && this.audioGainNode) {
            // Replace the stream with a new MediaStream which is
            // connected to the original stream via a GainNode.
            // This allows us to modify the volume of the audio as sent
            // to the other party.
            var audioContext = this.audioGainNode.context;
            var source = audioContext.createMediaStreamSource(stream);
            var dest = audioContext.createMediaStreamDestination();
            source.connect(this.audioGainNode);
            this.audioGainNode.connect(dest);
            stream = dest.stream;
        }

        return stream;
    };

    /**
     * Retrieves the Call-ID from a SIP message, and triggers "the initialized"
     * event when the call-id is found (if the call has not been terminated).
     * When the this.call_id has been set, the event is not triggered again.
     *
     * @private
     * @param {JsSIP.IncomingMessage} message
     * @fires PhoneCall#initialized
     */
    PhoneCall.prototype.receiveSIPMessage = function(message) {
        if (!message) {
            return;
        }
        var call_id = message.getHeader('X-CY-Call-ID');
        if (!call_id) {
            if (this.terminated) {
                // If the call has ended, then the server might not have given
                // any Call ID.
                return;
            }
            assert(!!this.call_id,
                    'phoneCall.call_id must be set if X-CY-Call-ID is missing');
            return;
        }
        if (this.call_id) {
            assert(call_id == this.call_id,
                    'Call ID should not change during a call!');
            return;
        }
        this.call_id = call_id;

        var my_leg_uuid = message.getHeader('X-CY-Leg-UUID');
        var other_leg_uuid = message.getHeader('X-CY-Other-Leg-UUID');
        // These variables should be set by Freeswitch.
        if (!my_leg_uuid)
            console.warn('Leg-UUID not set!');
        if (!other_leg_uuid)
            console.warn('Other-Leg-UUID not set!');
        this.my_leg_uuid = my_leg_uuid || '';
        this.other_leg_uuid = other_leg_uuid || '';

        if (!this.terminated) {
            var extraData = {
                request: new IncomingRequest(message)
            };
            emitEvent(this, 'initialized', this, extraData);
        }
    };

    /**
     * Triggered before applying a remote SDP to the local description,
     * and before sending off a local SDP to the SIP server.
     *
     * @private
     * @param {JsSIP.Event} event
     */
    PhoneCall.prototype.rtcSession_onsdp = function(event) {
        if (event.originator === 'local') {
            // Modify local SDP (sent to the remote party) to work around bugs.
            // This was originally added when Chrome 41 first started supporting
            // IPv6 addresses in the SDP, which resulted in "no audio" calls.
            // See 5486acf8bd03e5d31265ecce5218f2b2a7855cc1.
            //
            // Remove all candidates with IPv6 addresses.
            // NOTE: This hack prevents the library from working in IPv6-only
            // networks.
            // TODO: Is this work-around still needed?
            event.sdp = event.sdp.replace(
                /\r\na=candidate:.+ [0-9A-Fa-f:]+:[0-9A-Fa-f]* \d+ typ.+/g, '');
        }
        if (event.originator === 'remote') {
            // "mid" attribute is required. Add it if missing from remote SDP.
            // https://www.fxsitecompat.com/docs/2018/webrtc-sdp-offer-now-requires-mid-property
            // https://bugzilla.mozilla.org/show_bug.cgi?id=1495569
            // https://bugs.chromium.org/p/chromium/issues/detail?id=901787
            if (supportsUnifiedPlan() && !event.sdp.includes('\r\na=mid:')) {
                var sectionIndex = 0;
                event.sdp = event.sdp.replace(/\r\nm=[^\r\n]+/g, function(m) {
                    return m + '\r\na=mid:' + (sectionIndex++);
                });
            }
        }
    };

    /**
     * Triggered whenever an ICE candidate is gathered.
     *
     * @private
     * @param {JsSIP.Event} event
     */
    PhoneCall.prototype.rtcSession_onicecandidate = function(event) {
        // Stop ICE gathering and continue with the existing candidates after
        // receiving at least one host candidate. This avoids long delays in
        // call set up. Don't set a value that is too small, otherwise STUN
        // does not have enough time to complete and only host candidates will
        // be available.
        //
        // Without this, setting up the call can easily take 10 seconds, while
        // the browser waits for replies from a STUN server. This logic ensures
        // that call setups complete within a reasonable timeframe.
        clearTimeout(this._iceTimeout);
        this._iceTimeout = setTimeout(event.ready, 1000);
    };

    /**
     * Triggered when the call has been acknowledged.
     *
     * @private
     * @param {JsSIP.Event} event
     * @fires PhoneCall#outgoing_progress
     */
    PhoneCall.prototype.rtcSession_onprogress = function(event) {
        if (event.originator == 'remote') {
            this.receiveSIPMessage(event.response);
            emitEvent(this, 'outgoing_progress', this);
        }
    };

    /**
     * Triggered when audio starts flowing between server and client.
     *
     * @private
     * @param {JsSIP.Event} event
     * @fires PhoneCall#answered
     */
    PhoneCall.prototype.rtcSession_onaccepted = function(event) {
        this.receiveSIPMessage(event.response);
        var remoteStream = new MediaStream();
        var pc = this.rtcSession.connection;
        pc.getReceivers().forEach(function(receiver) {
            remoteStream.addTrack(receiver.track);
        });
        // Somehow, stream changes are not propagated to the track.
        // So instead of using the same track with the expectation that the
        // incoming audio continues to flow well, use the new stream.
        pc.addEventListener('track', function(event) {
            assert(event.streams.length === 1, 'Expect 1 stream in the track');
            this.incomingAudio.srcObject = event.streams[0];
        }.bind(this));
        this.incomingAudio.srcObject = remoteStream;
        emitEvent(this, 'answered', this);
    };

    /**
     * Triggered when a call fails to establish.
     *
     * @private
     * @param {JsSIP.Event} event
     * @fires PhoneCall#ended
     */
    PhoneCall.prototype.rtcSession_onfailed = function(event) {
        if (!this.rtcSession) {
            // Failure after call has already ended.
            return;
        }
        this.terminated = true;
        this.receiveSIPMessage(event.message);
        var extraData = {};
        if (event.cause === JsSIP.C.causes.SIP_FAILURE_CODE) {
            // A non-standard error has occurred.
            // Try hard to get some more useful information.
            extraData.error = event.message.reason_phrase +
                '(' + event.message.status_code + ')';
        } else {
            extraData.error = event.cause;
        }

        // Add an error code if it is meant to be public.
        if (event.cause) {
            for (var errorCode in exports.ERROR) {
                if (exports.ERROR[errorCode] === event.cause) {
                    extraData.errorCode = errorCode;
                    break;
                }
            }
        }

        emitEvent(this, 'ended', this, extraData);
        this.rtcSession = null;
    };

    /**
     * Triggered when an established call ends.
     * @param {JsSIP.Event} event
     *
     * @private
     * @fires PhoneCall#ended
     */
    PhoneCall.prototype.rtcSession_onended = function(event) {
        this.terminated = true;
        this.receiveSIPMessage(event.message);
        emitEvent(this, 'ended', this);
        this.rtcSession = null;
    };

    /**
     * Triggered when a DTMF is received.
     *
     * @private
     * @param {JsSIP.Event} event
     * @fires PhoneCall#dtmf
     */
    PhoneCall.prototype.rtcSession_ondtmf = function(event) {
        this.receiveSIPMessage(event.request);
        var extraData = {
            dtmf: event.dtmf.tone,
            isRemote: event.originator == 'remote'
        };
        emitEvent(this, 'dtmf', this, extraData);
    };

    /**
     * Triggered when the hold state changes.
     *
     * @private
     * @fires PhoneCall#holdchanged
     */
    PhoneCall.prototype.rtcSession_onholdchanged = function() {
        emitEvent(this, 'holdchanged', this);
    };

    /**
     * Triggered when a custom event was received.
     *
     * @private
     * @param {JsSIP.Event} event
     * @param {string} event.type - type
     * @param {JsSIP.IncomingRequest} event.request
     * @fires PhoneCall#holdchanged
     */
    PhoneCall.prototype.rtcSession_oncy_info = function(event) {
        switch (event.type) {
        case 'set_call_id':
            var call_id = event.request.getHeader('X-CY-Call-ID');
            var previous_call_id = this.call_id;
            // Only set the call ID if it is specified and the initialized
            // event was already triggered.
            if (call_id && previous_call_id && call_id !== previous_call_id) {
                this.call_id = call_id;
                emitEvent(this, 'call_id_changed', this, {
                    previous_call_id: previous_call_id
                });
            }
            break;
        default:
            if (exports.debugEnabled) {
                console.warn('Unknown INFO msg: ' + event.type, event.request);
            }
        }
    };

    /**
     * Request info to be exposed on the Phone#incoming and Phone#initialized
     * event.
     *
     * @param {JsSIP.IncomingMessage} incomingMessage
     * @private
     */
    function IncomingRequest(incomingMessage) {
        var request = {};
        /**
         * Returns the value of the first header that has a case-insensitive
         * match for the given name, null otherwise.
         *
         * @public
         * @param {string} name - Name of request header
         * @return {string|null} The header 
         */
        request.getHeader = function(name) {
            return incomingMessage.getHeader(name);
        };
        return request;
    }

    /**
     * Bind all of the prototype methods to `self`, i.e. `this` in every method
     * is `self`.
     *
     * @private
     */
    function bindAllInstanceMethods(self) {
        Object.keys(self.constructor.prototype).forEach(function(methodName) {
            var method = self[methodName];
            if (typeof self[methodName] == 'function') {
                self[methodName] = method.bind(self);
            }
        });
    }

    function assert(condition, message) {
        if (!condition) {
            throw new Error('Assertion failed: ' + message);
        }
    }

    // The function that is responsible for handling all getUserMedia calls in
    // this library. This may be overridden by overrideGetUserMedia.
    var callGetUserMedia = function(mediaConstraints) {
        return navigator.mediaDevices.getUserMedia(mediaConstraints);
    };

    /**
     * Override the getUserMedia method used that is used to get an audio
     * stream.
     *
     * @param {function=} getUserMediaImpl - A function that takes the same
     *   parameters as navigator.mediaDevices.getUserMedia.
     */
    function overrideGetUserMedia(getUserMediaImpl) {
        if (typeof getUserMediaImpl != 'function') {
            getUserMediaImpl = wrappedGetUserMedia;
        }
        callGetUserMedia = getUserMediaImpl;
    }

    function MediaStream_stopPolyfill() {
        var tracks = this.getTracks();
        for (var i = 0; i < tracks.length; ++i) {
            tracks[i].stop();
        }
    }

    function getPeerConnectionConfig() {
        var pcConfig = {
            // TODO: Add a TURN server to make sure that media always flows,
            // even when the client is behind an aggressive firewall.
            iceServers: [{
                urls: 'stun:stun.l.google.com:19302',
            }],
            // Use the common standard SDP semantics to be future-proof.
            // https://webrtc.org/web-apis/chrome/unified-plan/
            sdpSemantics: 'unified-plan',
        };
        if (!supportsUnifiedPlan()) {
            delete pcConfig.sdpSemantics;
        }
        return pcConfig;
    }

    // Feature detection to support Chrome 68 and earlier.
    function supportsUnifiedPlan() {
        // Chrome used to only support a non-standard-compliant SDP format,
        // called "Plan B". The "unified plan" is supported in Chrome 69,
        // the same version where RTCRtpTransceiver was added.
        return !window.chrome || !!window.RTCRtpTransceiver;
    }

    JsSIP.debug.log = function(format) {
        var fullMsg = [].join.call(arguments);
        var isError = /error[: ]/i.test(fullMsg);
        // Always log errors to make sure that these get fixed.
        var shouldLog = exports.debugEnabled || isError;
        if (shouldLog) {
            var logger = console.log;
            if (isError || fullMsg.split('\n', 2).length !== 2) {
                // Use console.error to get clickable stack traces in Chrome's
                // devtools. Restrict this to errors and single lines, to avoid
                // using console.error to log huge SIP messages.
                logger = console.error;
            }
            logger.apply(console, arguments);
        }
    };

    // The number of audio contexts per document is limited. Export the
    // AudioContext instance so that others can re-use the object, and/or
    // detect whether input volume can be controlled.
    //
    // AudioContext instances are initialized in a suspended state, until
    // a user gesture has occurred. "audioContext" will lazily be initialized
    // upon the first user gesture, if possible (see below).
    Object.defineProperty(exports, 'audioContext', {
        configurable: true,
        enumerable: true,
        // Lazily instantiate audioContext, so that constructing an
        // AudioContext does not cause any warnings.
        get() {
            try {
                this.audioContext = new AudioContext();
            } catch (e) {
                this.audioContext = null;
            }
            return this.audioContext;
        },
        set(v) {
            delete this.audioContext;
            this.audioContext = v;
        },
    });
    (function() {
        function unlockAudio() {
            // Triggers lazy initialization of AudioContext if needed.
            var ac = exports.audioContext;
            // Resume AudioContext, see https://developers.google.com/web/updates/2017/09/autoplay-policy-changes
            if (ac && ac.state === 'suspended') {
                ac.resume();
            }
            document.removeEventListener('click', unlockAudio, true);
            document.removeEventListener('touchstart', unlockAudio, true);
            document.removeEventListener('touchend', unlockAudio, true);
        }
        document.addEventListener('click', unlockAudio, true);
        document.addEventListener('touchstart', unlockAudio, true);
        document.addEventListener('touchend', unlockAudio, true);
    })();

    // Whether to print detailed debug messages from JsSIP.
    exports.debugEnabled = false;

    /**
     * Public error codes. These can be used to find specific errors in the
     * "ended" event. Simply check whether event.errorCode is equal to one
     * of the errors listed below.
     *
     * These are the supported error codes (sorted alphabetically). More error
     * codes are listed in JsSIP (jssip.js) at "causes: {"
     */
    exports.ERROR = {
        ADDRESS_INCOMPLETE: JsSIP.C.causes.ADDRESS_INCOMPLETE,
        BUSY: JsSIP.C.causes.BUSY,
        CANCELED: JsSIP.C.causes.CANCELED,
        NO_ANSWER: JsSIP.C.causes.NO_ANSWER,
        NOT_FOUND: JsSIP.C.causes.NOT_FOUND,
        REJECTED: JsSIP.C.causes.REJECTED,
        UNAVAILABLE: JsSIP.C.causes.UNAVAILABLE,
        // The user denied access to the microphone. Use this event to educate
        // the user about enabling the microphone.
        USER_DENIED_MEDIA_ACCESS: JsSIP.C.causes.USER_DENIED_MEDIA_ACCESS,
    };

    exports.isSupported = !!window.RTCPeerConnection;
    exports.overrideGetUserMedia = overrideGetUserMedia;
    exports.wrappedGetUserMedia = wrappedGetUserMedia;

    exports.Phone = Phone;
    exports.PhoneCall = PhoneCall;
    exports.PhoneSounds = PhoneSounds;
    return exports;
});
