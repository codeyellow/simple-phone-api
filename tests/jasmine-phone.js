/* globals phone */
phone.Phone.prototype.jasmineToString = function() {
    'use strict';
    var options = this.options;
    var id = options && (options.username + '@' + options.sip_domain);
    return 'Phone(' + id + ')';
};

phone.PhoneCall.prototype.jasmineToString = function() {
    'use strict';
    return 'PhoneCall(' + this.call_id + ', ' + this.my_leg_uuid + ')';
};
