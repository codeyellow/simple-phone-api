/* globals describe, it, afterEach, beforeAll, beforeEach, expect, jasmine,
           spyOn, phone, Promise, _, Symbol, xit */

// Note: If any test fails, the easiest way to debug the issue is to open the
// debugger, and put a breakpoint at the failing test. Then do something like
// pSpy.getCalls() and look at the arguments.

describe('PhoneCall', function() {
    'use strict';

    // use Symbol for constants to avoid accidental use of the constant's value.
    // A nice side effect of this is that the test won't run in ancient browsers
    // because Symbols are only supported in the latest version of browsers that
    // support WebRTC: Chrome 38+, Firefox 36+, Opera 25+, IE Edge.
    var USER_CALLER = Symbol('caller');
    var USER_CALLEE = Symbol('callee');
    var USER_CALLEE2 = Symbol('another callee');
    var USER_AUTO_ANSWER = Symbol('auto answer');
    var USER_INVALID = Symbol('non-existent user');
    var USER_EAVESDROPPER = Symbol('eavesdropper');

    // Maximum allowed time before a test times out...
    // ... extra margin on top of the other timeouts, to allow assertions to run
    // and complete. Also useful to allow an arbitrary test duration, e.g. when
    // you want to debug using the 'debugger' statement which pauses execution.
    var TIMEOUT_ALWAYS = 500;
    // ... to process a fake gUM call (independent of network).
    var TIMEOUT_LOCAL_GUM = 500;
    // ... to connect to SIP server over the WebSocket.
    var TIMEOUT_CONNECT = 3000;
    // ... to setup a call which gets rejected by the SIP server.
    var TIMEOUT_CONNECT_AND_REJECT = TIMEOUT_CONNECT + 2000;
    // ... to setup and answer the phone call (automatically by server).
    var TIMEOUT_CONNECT_AND_ANSWER = TIMEOUT_CONNECT + 3000;
    // ... to bridge two calls (phone calls other phone which answers).
    var TIMEOUT_BRIDGE = 7000;
    // ... to wait until a hold request is acknowledged.
    var TIMEOUT_HOLD_CHANGE = 3000;
    // ... to allow the JsSIP library to close the WebSocket.
    // JsSIP calls .disconnect() after 2 seconds in UA.prototype.stop. So let's
    // wait 2.2 seconds to allow the library to exchange some messages before
    // disconnecting.
    var TIMEOUT_DISCONNECT_SOCKET = 2200;
    // ... time-out before assuming that the answer request was received and
    // processed by the other end. This is not a maximum timeout, tests will
    // actually wait for so much time before continuing with the next step, so
    // don't choose an unnecessarily large value.
    var TIMEOUT_ASSUME_ANSWER_COMPLETED = 2000;
    // ... time-out before assuming that the call should really have ended due
    // to termination of the other call leg.
    var TIMEOUT_ASSUME_HANGUP_PROPAGATED = 1000;

    // Store resolved numbers here so that they remain constant within a test.
    var _users_cache = {};

    // Only used by getUsernameImpl.
    var _available_numbers = ['*3333', '*5555', '*7777', '*9999'];
    var _available_numbers_counter = 0;

    function getNumber(number) {
        return getUsername(number); // identical to username at the moment.
    }
    function getUsername(user_enum_value) {
        var username = _users_cache[user_enum_value];
        return username ||
            (_users_cache[user_enum_value] = getUsernameImpl(user_enum_value));
    }
    // Called when a new phone number must tbe generated. May return a different
    // result on each call, as long as it can be used for the given purpose.
    function getUsernameImpl(user_enum_value) {
        switch (user_enum_value) {
        case USER_CALLER:
            // Can be constant, because it doesn't matter if the previous user
            // is still registered.
            return '*1111';
        case USER_EAVESDROPPER:
            // Return same value as caller, because this number is not called,
            // and the act of eavesdropping should not affect the call.
            return getUsernameImpl(USER_CALLER);
        case USER_CALLEE:
        case USER_CALLEE2:
            // Cycle through the list of test numbers to minimize interference
            // between tests. E.g. when the previous test did not properly
            // unregister a user, the next test should not be affected by it.
            return _available_numbers[
                _available_numbers_counter++ % _available_numbers.length];
        case USER_AUTO_ANSWER:
            // Implemented by PBX: Responds immediately.
            return '*007';
        case USER_INVALID:
            // This number can never be called and there is no such user either.
            return '*invalid-username';
        default:
            throw new Error('Invalid user enum: ' +
                    (user_enum_value && user_enum_value.toString()));
        }
    }
    function getPhoneConfig(user_enum_value) {
        console.assert(user_enum_value, 'user must be specified!');
        var username = getUsername(user_enum_value);
        // TODO: Dynamic domains ? Concurrent tests are not supported atm...
        return {
            username: username,
            password: 'justincase',
            sip_domain: 'testing.test',
            //ws_servers: ['wss://fs-test0:8088/'],
            ws_servers: ['wss://tilaa1.pbx.letstalkrex.com:8088/'],
            //ws_servers: ['wss://pbx.incenova.com:8088/'],
        };
    }
    function getCalls(spy) {
        return spy && spy.calls.allArgs().map(function(args) {
            return args[0];
        });
    }

    function promiseRegistration(aPhone) {
        return new Promise(function(resolve) {
            // TODO: Add 'registered' event to public API and use that instead.
            aPhone.jssipPhone.once('registered', function() {
                resolve();
            });
            aPhone.jssipPhone.once('registrationFailed', function() {
                console.warn('Registration failed for ', aPhone);
                resolve();
            });
        });
    }

    // Use this method to wait until the phone call has ended. Its main purpose
    // is to make sure that test expectations are always checked, even if the
    // test times out.
    function onEndedOrDestroyed(myphone, phoneCall, callback) {
        console.assert(myphone instanceof phone.Phone);
        console.assert(phoneCall instanceof phone.PhoneCall);
        console.assert(typeof callback === 'function');
        // This should not happen. After all, if you know that the call has
        // ended, then you could immediately call the callback.
        if (phoneCall.terminated) {
            console.warn('Phone call has already been terminated!');
            callback();
            return;
        }

        // This should definitely not happen, and most likely means that the
        // expectations are registered when the test has already ended (and
        // afterEach has destroyed the phone).
        if (!myphone.jssipPhone) {
            console.warn('Phone is already destroyed');
            callCallbackWhenEndedOrDestroyed();
            return;
        }

        // This SHOULD happen - the call completes normally
        phoneCall.once('ended', callCallbackWhenEndedOrDestroyed);

        // This should not happen, and the main cause if it happens is that a
        // test times out and the afterEach hook destroys the phone.
        myphone.once('destroyed', function() {
            if (callback) {
                console.warn('Phone was destroyed before call was ended!');
                callCallbackWhenEndedOrDestroyed();
            }
        });

        function callCallbackWhenEndedOrDestroyed() {
            if (callback) {
                var cb = callback;
                callback = null;
                cb();
            }
        }
    }

    var _createdPhones = [];
    function createPhone(config) {
        var newPhone = new phone.Phone(config);
        _createdPhones.push(newPhone);
        return newPhone;
    }

    beforeAll(function() {
        jasmine.addMatchers({
            toBeEvent: function(util, customEqualityTesters) {
                return {
                    compare: function(actual, expected) {
                        if (expected.type && actual[0] !== expected.type) {
                            return {
                                pass: false,
                            };
                        }
                        // We only care about event keys that were specified.
                        // TODO: Test all properties instead of a subset?
                        var event = _.pick(actual[1], _.keys(expected));
                        return {
                            pass: util.equals(event, expected,
                                              customEqualityTesters),
                        };
                    },
                };
            },
        });
    });

    beforeEach(function(done) {
        _users_cache = {}; // Force generation of new phone numbers
        done();
    });
    afterEach(function(done) {
        var phoneCallCounts = _createdPhones.map(function(aPhone, i) {
            var phoneCallCount = aPhone.phoneCalls.length;
            aPhone.destroy();
            return phoneCallCount;
        });
        _createdPhones.length = 0;

        // Sanity check: After each test, the call should have ended.
        phoneCallCounts.forEach(function(phoneCallCount, i) {
            expect(phoneCallCount + ' open phoneCalls for phone ' + i)
                .toBe('0 open phoneCalls for phone ' + i);
        });

        phone.overrideGetUserMedia();

        done();
    });

    it('should recognize WebRTC support', function() {
        expect(phone.isSupported).toBe(true);
    }, TIMEOUT_ALWAYS);

    it('should export error constants with unique values', function() {
        // The exact value doesn't matter; they should only be unique strings.
        Object.keys(phone.ERROR).forEach(function(errorCode) {
            expect(errorCode).toMatch(/^[A-Z_]+$/);
            expect(phone.ERROR[errorCode]).toEqual(jasmine.any(String));
        });
        expect(phone.ERROR).toEqual({
            ADDRESS_INCOMPLETE: 'Address Incomplete',
            BUSY: 'Busy',
            CANCELED: 'Canceled',
            NOT_FOUND: 'Not Found',
            NO_ANSWER: 'No Answer',
            REJECTED: 'Rejected',
            UNAVAILABLE: 'Unavailable',
            USER_DENIED_MEDIA_ACCESS: 'User Denied Media Access',
        });
    }, TIMEOUT_ALWAYS);

    it('should allow getUserMedia override and handle deny', function(done) {
        var gumSpy = jasmine.createSpy('gumOverride').and.callFake(gumOverride);
        async function gumOverride(constraints) {
            expect(constraints).toEqual({
                audio: true,
                video: false,
            });
            expect(arguments.length).toEqual(1);
            throw new Error('denied');
        }
        phone.overrideGetUserMedia(gumSpy);
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        // We don't care about the recipient.
        var phoneCall = myphone.startCall(getNumber(USER_INVALID));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        expect(phone.ERROR.USER_DENIED_MEDIA_ACCESS).toEqual(
                jasmine.any(String));

        onEndedOrDestroyed(myphone, phoneCall, function() {
            // We don't care whether there is a 'connected'/'disconnected'
            // event.
            expect(getCalls(pSpy)).toContain('created');
            expect(getCalls(pcSpy)).not.toContain('initialized');
            expect(getCalls(pcSpy)).toContain('ended');
            if (pcSpy.calls.mostRecent()) {
                expect(pcSpy.calls.mostRecent().args).toBeEvent({
                    type: 'ended',
                    phoneCall: phoneCall,
                    error: phone.ERROR.USER_DENIED_MEDIA_ACCESS,
                });
            } else {
                expect(pcSpy).toHaveBeenCalled();
            }

            expect(gumSpy.calls.count()).toBe(1);

            done();
        });
    }, TIMEOUT_LOCAL_GUM + TIMEOUT_ALWAYS);

    it('should support setting/getting microphone volume', function() {
        var phoneCall = new phone.PhoneCall();
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        expect(phoneCall.getInputVolume()).toBe(1);
        phoneCall.setInputVolume(0.5);
        expect(phoneCall.getInputVolume()).toBe(0.5);
        phoneCall.setInputVolume(0);
        expect(phoneCall.getInputVolume()).toBe(0);
        phoneCall.setInputVolume(1);
        expect(phoneCall.getInputVolume()).toBe(1);

        expect(function() {
            phoneCall.setInputVolume(-1);
        }).toThrow();

        expect(function() {
            phoneCall.setInputVolume(2);
        }).toThrow();

        expect(pcSpy).not.toHaveBeenCalled();
    }, TIMEOUT_ALWAYS);

    it('should support setting/getting speaker volume', function() {
        var phoneCall = new phone.PhoneCall();
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        expect(phoneCall.getVoiceVolume()).toBe(1);
        phoneCall.setVoiceVolume(0.5);
        expect(phoneCall.getVoiceVolume()).toBe(0.5);
        phoneCall.setVoiceVolume(0);
        expect(phoneCall.getVoiceVolume()).toBe(0);
        phoneCall.setVoiceVolume(1);
        expect(phoneCall.getVoiceVolume()).toBe(1);

        expect(function() {
            phoneCall.setVoiceVolume(-1);
        }).toThrow();

        expect(function() {
            phoneCall.setVoiceVolume(2);
        }).toThrow();

        expect(pcSpy).not.toHaveBeenCalled();
    }, TIMEOUT_ALWAYS);

    it('should connect', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();
        // Being connected to the WebSocket immediately after creation is not
        // likely.
        expect(myphone.connected).toBeFalsy();
        // TODO: Investigate why "connected" happens even when there is no
        // connection...
        myphone.once('connected', function() {
            expect(myphone.connected).toBe(true);
            // In particular, there should be no 'disconnected' event.
            expect(getCalls(pSpy)).toEqual(['connected']);
            done();
        });
    }, TIMEOUT_CONNECT + TIMEOUT_ALWAYS);

    it('should disconnect for a non-existent server', function(done) {
        var phoneConfig = getPhoneConfig(USER_CALLER);
        phoneConfig.ws_servers = ['wss://0.0.0.0:0/certainlyinvalid'];
        var myphone = createPhone(phoneConfig);
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();
        expect(myphone.connected).toBeFalsy();
        myphone.on('disconnected', function() {
            expect(myphone.connected).toBe(false);
            // In particular, there should be no 'connected' event.
            expect(getCalls(pSpy)).toEqual(['disconnected']);
            done();
        });
    }, TIMEOUT_CONNECT + TIMEOUT_ALWAYS);

    it('should reject a call to an invalid number', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var phoneCall = myphone.startCall(getNumber(USER_INVALID));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        onEndedOrDestroyed(myphone, phoneCall, function() {
            // 'connect' happens after 'created' because the created event is
            // dispatched as soon as the 'PhoneCall' instance has been created,
            // even if there is no phone.
            expect(getCalls(pSpy)).toEqual(['created', 'connected']);
            expect(pSpy.calls.argsFor(0)).toBeEvent({
                type: 'created',
                phoneCall: phoneCall,
            });

            expect(getCalls(pcSpy)).toEqual(['ended']);
            expect(pcSpy.calls.argsFor(0)).toBeEvent({
                type: 'ended',
                phoneCall: phoneCall,
            });

            done();
        });
    }, TIMEOUT_CONNECT_AND_REJECT + TIMEOUT_ALWAYS);

    it('should reject a call for an invalid user account', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_INVALID));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var phoneCall = myphone.startCall(getNumber(USER_CALLEE));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        onEndedOrDestroyed(myphone, phoneCall, function() {
            expect(getCalls(pSpy)).toEqual(['created', 'connected']);

            expect(getCalls(pcSpy)).toEqual(['ended']);

            done();
        });
    }, TIMEOUT_CONNECT_AND_REJECT + TIMEOUT_ALWAYS);

    it('should start a call that gets accepted (echo test)', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var phoneCall = myphone.startCall(getNumber(USER_AUTO_ANSWER));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        phoneCall.once('initialized', function() {
            expect(myphone.connected).toEqual(true);
            expect(getCalls(pSpy)).toContain('connected');
        });

        phoneCall.once('answered', function() {
            phoneCall.terminate();
        });

        onEndedOrDestroyed(myphone, phoneCall, function() {
            expect(getCalls(pSpy)).toEqual(['created', 'connected']);

            // NOTE: The echo test immediately answers, so there is no 'ringing'
            // event (outgoing_progress) after 'initialized'.
            expect(getCalls(pcSpy)).toEqual(['initialized', 'answered',
                    'ended']);

            expect(pcSpy.calls.argsFor(0)).toBeEvent({
                type: 'initialized',
                phoneCall: phoneCall,
            });
            expect(pcSpy.calls.argsFor(1)).toBeEvent({
                type: 'answered',
                phoneCall: phoneCall,
            });
            expect(pcSpy.calls.argsFor(2)).toBeEvent({
                type: 'ended',
                phoneCall: phoneCall,
            });

            done();
        });
    }, TIMEOUT_CONNECT_AND_ANSWER + TIMEOUT_ALWAYS);

    // This is the most extensive test, because it is the most common scenario.
    it('should get an incoming call and be able to answer it', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();
        var myphone2 = createPhone(getPhoneConfig(USER_CALLEE));
        var pSpy2 = spyOn(myphone2, 'trigger').and.callThrough();

        var pcSpy;
        var pcSpy2;
        var phoneCall;
        var phoneCall2;

        Promise.all([
            promiseRegistration(myphone),
            promiseRegistration(myphone2),
        ]).then(startTest);

        myphone2.once('created', function(event) {
            phoneCall2 = event.phoneCall;
            pcSpy2 = spyOn(phoneCall2, 'trigger').and.callThrough();
            phoneCall2.once('initialized', function() {
                expect(phoneCall2.call_id).toEqual(jasmine.any(String));
                expect(phoneCall2.my_leg_uuid).toEqual(jasmine.any(String));
                expect(phoneCall2.other_leg_uuid).toEqual(jasmine.any(String));
                expect(phoneCall2.terminated).toBe(false);
                expect(phoneCall2.incoming).toBe(true);
                expect(phoneCall2.answered).toBe(false);
            });
        });
        myphone2.once('incoming', function(event) {
            // phoneCall2 should be set in the 'created' event.
            expect(phoneCall2).not.toBeUndefined();
            expect(event.phoneCall).toBe(phoneCall2);
            phoneCall2.answer();

            // These must not set until the 'initialized' event is triggered,
            // even if we can technically get the properties. This gives us the
            // freedom to send the incoming event as soon as possible, even
            // without knowing the call_id yet.
            expect(phoneCall2.call_id).toBeNull();
            expect(phoneCall2.my_leg_uuid).toBeNull();
            expect(phoneCall2.other_leg_uuid).toBeNull();

            expect(phoneCall2.terminated).toBe(false);
            expect(phoneCall2.incoming).toBe(true);
            expect(phoneCall2.answered).toBe(false);
        });


        function startTest() {
            phoneCall = myphone.startCall(getNumber(USER_CALLEE));
            pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();
            onEndedOrDestroyed(myphone, phoneCall, checkExpectations);
            phoneCall.on('answered', function() {
                expect(phoneCall.answered).toBe(true);
                expect(phoneCall.terminated).toBe(false);

                // 'answered' should only be triggered if the callee picked up.
                expect(getCalls(pcSpy2)).toContain('answered');
                expect(phoneCall2.answered).toBe(true);
                phoneCall2.terminate();
                expect(phoneCall2.terminated).toBe(true);
            });
            phoneCall.once('initialized', function() {
                expect(phoneCall.call_id).toEqual(jasmine.any(String));
                expect(phoneCall.my_leg_uuid).toEqual(jasmine.any(String));
                // Other leg ID should only be set for incoming calls, because
                // you never know who (how many?) is talking on the other end.
                expect(phoneCall.other_leg_uuid).toBe('');
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall.incoming).toBe(false);
                expect(phoneCall.answered).toBe(false);
            });
        }

        function checkExpectations() {
            expect(getCalls(pSpy)).toEqual(['connected', 'created']);

            expect(getCalls(pSpy2)).toEqual(['connected', 'created',
                    'incoming']);

            expect(getCalls(pcSpy)).toEqual(['initialized', 'outgoing_progress',
                    'answered', 'ended']);
            expect(getCalls(pcSpy2)).toEqual(['initialized', 'answered',
                    'ended']);

            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall.answered).toBe(true);

            if (!phoneCall2) {
                done();
                return;
            }
            // The next tests require phoneCall2 to be set.

            expect(phoneCall.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall.my_leg_uuid).toEqual(phoneCall2.other_leg_uuid);
            expect(phoneCall.my_leg_uuid).not.toEqual(phoneCall2.my_leg_uuid);

            done();
        }
    }, TIMEOUT_CONNECT + TIMEOUT_BRIDGE + TIMEOUT_ALWAYS);

    it('should not start calls when the phone is destroyed', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var phoneCall = myphone.startCall(getNumber(USER_AUTO_ANSWER));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        Promise.all([
            new Promise(function(resolve) {
                myphone.once('destroyed', resolve);
                // Should be called as soon as .destroy() is called (this is
                // also tested below, right after calling .destroy()).
                setTimeout(resolve, 50, null);
            }),
            new Promise(function(resolve) {
                myphone.once('disconnected', resolve);
                setTimeout(resolve, TIMEOUT_DISCONNECT_SOCKET, null);
            }),
            new Promise(function(resolve) {
                phoneCall.once('ended', resolve);
                // getUserMedia may take some time.
                setTimeout(resolve, TIMEOUT_LOCAL_GUM, null);
            }),
        ]).then(function(events) {
            expect(events[0]).toEqual(jasmine.objectContaining({
                type: 'destroyed',
                phone: myphone,
            }));
            expect(events[1]).toEqual(jasmine.objectContaining({
                type: 'disconnected',
                // TODO: Should there be a phone property here?
            }));
            expect(events[2]).toEqual(jasmine.objectContaining({
                type: 'ended',
                phoneCall: phoneCall,
            }));
            // Should not bother with connecting because the phone was destroyed
            // right after it was created.
            expect(getCalls(pSpy)).not.toContain('connected');
            expect(getCalls(pcSpy)).toEqual(['ended']);
            done();
        });
        myphone.destroy();
        expect(getCalls(pSpy)).toContain('destroyed');
        // At the moment, the implementation only clears the phone call after
        // the user media request etc. have completed. Until then, phoneCalls
        // will still contain the PhoneCall, even if the Phone is destroyed.
        expect(myphone.phoneCalls).toEqual([phoneCall]);
    }, TIMEOUT_DISCONNECT_SOCKET + TIMEOUT_LOCAL_GUM + TIMEOUT_ALWAYS);

    it('should drop phone calls when the phone is destroyed', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var phoneCall = myphone.startCall(getNumber(USER_AUTO_ANSWER));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        var answeredPromise = new Promise(function(resolve) {
            phoneCall.once('answered', resolve);
        });
        Promise.all([
            new Promise(function(resolve, reject) {
                answeredPromise.then(resolve);
                // The server should quickly respond to echo calls.
                setTimeout(reject, TIMEOUT_CONNECT_AND_ANSWER,
                        new Error('The call was not answered!'));
            }),
            new Promise(function(resolve) {
                myphone.once('destroyed', resolve);
            }),
            new Promise(function(resolve) {
                myphone.once('disconnected', resolve);
                answeredPromise.then(function() {
                    setTimeout(resolve, TIMEOUT_DISCONNECT_SOCKET, null);
                });
            }),
            new Promise(function(resolve) {
                phoneCall.once('ended', resolve);
                answeredPromise.then(function() {
                    setTimeout(resolve, TIMEOUT_DISCONNECT_SOCKET, null);
                });
            }),
        ]).then(function(events) {
            expect(events[1]).toEqual(jasmine.objectContaining({
                type: 'destroyed',
                phone: myphone,
            }));
            expect(events[2]).toEqual(jasmine.objectContaining({
                type: 'disconnected',
                // TODO: Should there be a phone property here?
            }));
            expect(events[3]).toEqual(jasmine.objectContaining({
                type: 'ended',
                phoneCall: phoneCall,
            }));
        }, function(error) {
            expect(error).toBe('no error');
        }).then(function() {
            // Note: The documentation states that .destroy() allows destroyed /
            // disconnected / ended to be called in any order. Here, I expect
            // that disconnected happens after the phone was destroyed. If this
            // condition ever starts to fail due to a refactor, simply swap
            // the event order.
            expect(getCalls(pSpy)).toEqual(['created', 'connected', 'destroyed',
                    'disconnected']);
            expect(getCalls(pcSpy)).toEqual(['initialized', 'answered',
                    'ended']);
            done();
        });

        phoneCall.once('answered', function() {
            myphone.destroy();
            expect(getCalls(pSpy)).toContain('destroyed');
            expect(myphone.phoneCalls).toEqual([]);
        });
    }, TIMEOUT_CONNECT_AND_ANSWER + TIMEOUT_DISCONNECT_SOCKET + TIMEOUT_ALWAYS);

    it('should support call transfer of active incoming call', function(done) {
        // myphone will dial myphone2, which answers the call and transfers the
        // call to myphone3.
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var myphone2 = createPhone(getPhoneConfig(USER_CALLEE));
        var pSpy2 = spyOn(myphone2, 'trigger').and.callThrough();

        var myphone3 = createPhone(getPhoneConfig(USER_CALLEE2));
        var pSpy3 = spyOn(myphone3, 'trigger').and.callThrough();

        var pcSpy;
        var pcSpy2;
        var pcSpy3;
        var phoneCall;
        var phoneCall2;
        var phoneCall3;
        var testStatus = 'did not call transferCall yet!';

        Promise.all([
            promiseRegistration(myphone),
            promiseRegistration(myphone2),
            promiseRegistration(myphone3),
        ]).then(startTest);

        myphone2.once('incoming', function(event) {
            phoneCall2 = event.phoneCall;
            pcSpy2 = spyOn(phoneCall2, 'trigger').and.callThrough();

            phoneCall2.answer();
        });

        myphone3.once('incoming', function(event) {
            phoneCall3 = event.phoneCall;
            pcSpy3 = spyOn(phoneCall3, 'trigger').and.callThrough();

            expect(phoneCall3.terminated).toBe(false);
            expect(phoneCall3.incoming).toBe(true);
            expect(phoneCall3.answered).toBe(false);

            phoneCall3.answer();

            // TODO: Somehow check whether we are really talking with the
            // initial caller (audio analysis?). For now, we assume that the
            // transfer succeeded if the call is active for the caller,
            // dropped for the callee and active for the destination.
            setTimeout(function() {
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall2.terminated).toBe(true);
                expect(phoneCall3.terminated).toBe(false);
                testStatus = 'transferred call terminated by receiver';
                phoneCall3.terminate();
            }, TIMEOUT_ASSUME_ANSWER_COMPLETED);
        });

        function startTest() {
            phoneCall = myphone.startCall(getNumber(USER_CALLEE));
            pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();
            onEndedOrDestroyed(myphone, phoneCall, checkExpectations);

            phoneCall.on('answered', function() {
                expect(phoneCall.answered).toBe(true);
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall2.answered).toBe(true);
                expect(phoneCall2.terminated).toBe(false);
                expect(phoneCall3).toBeUndefined();

                // myphone and myphone2 are now talking. Let's transfer the call
                // from myphone2 to myphone3.
                testStatus = 'will call transferCall';
                phoneCall2.transferCall(getNumber(USER_CALLEE2));
                testStatus = 'called transferCall';
            });
        }

        function checkExpectations() {
            expect(testStatus).toBe('transferred call terminated by receiver');

            expect(getCalls(pSpy)).toEqual(['connected', 'created']);
            expect(getCalls(pSpy2)).toEqual(['connected', 'created',
                    'incoming']);
            expect(getCalls(pSpy3)).toEqual(['connected', 'created',
                    'incoming']);

            expect(getCalls(pcSpy)).toEqual(['initialized', 'outgoing_progress',
                    'answered', 'ended']);
            expect(getCalls(pcSpy2)).toEqual(['initialized', 'answered',
                    'ended']);
            expect(getCalls(pcSpy3)).toEqual(['initialized', 'answered',
                    'ended']);

            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall.answered).toBe(true);

            if (phoneCall2) {
                expect(phoneCall2.terminated).toBe(true);
                expect(phoneCall2.answered).toBe(true);
            }

            if (phoneCall3) {
                expect(phoneCall3.terminated).toBe(true);
                expect(phoneCall3.answered).toBe(true);
            }

            if (!phoneCall2 || !phoneCall3) {
                done();
                return;
            }

            expect(phoneCall.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall.my_leg_uuid).toEqual(phoneCall2.other_leg_uuid);
            expect(phoneCall.other_leg_uuid).not.toEqual(
                    phoneCall2.my_leg_uuid);

            expect(phoneCall3.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall.my_leg_uuid);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall2.my_leg_uuid);
            expect(phoneCall3.other_leg_uuid).toEqual(phoneCall.my_leg_uuid);

            done();
        }
    }, TIMEOUT_CONNECT + 2 * TIMEOUT_BRIDGE + TIMEOUT_ALWAYS);

    it('should support call transfer of active outgoing call', function(done) {
        // myphone will dial myphone2, which answers the call. Then myphone
        // transfers the call to myphone3.
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var myphone2 = createPhone(getPhoneConfig(USER_CALLEE));
        var pSpy2 = spyOn(myphone2, 'trigger').and.callThrough();

        var myphone3 = createPhone(getPhoneConfig(USER_CALLEE2));
        var pSpy3 = spyOn(myphone3, 'trigger').and.callThrough();

        var pcSpy;
        var pcSpy2;
        var pcSpy3;
        var phoneCall;
        var phoneCall2;
        var phoneCall3;
        var testStatus = 'did not call transferCall yet!';

        Promise.all([
            promiseRegistration(myphone),
            promiseRegistration(myphone2),
            promiseRegistration(myphone3),
        ]).then(startTest);

        myphone2.once('incoming', function(event) {
            phoneCall2 = event.phoneCall;
            pcSpy2 = spyOn(phoneCall2, 'trigger').and.callThrough();
            onEndedOrDestroyed(myphone2, phoneCall2, checkExpectations);

            phoneCall2.answer();
        });

        myphone3.once('incoming', function(event) {
            phoneCall3 = event.phoneCall;
            pcSpy3 = spyOn(phoneCall3, 'trigger').and.callThrough();

            expect(phoneCall3.terminated).toBe(false);
            expect(phoneCall3.incoming).toBe(true);
            expect(phoneCall3.answered).toBe(false);

            phoneCall3.answer();

            // TODO: Somehow check whether we are really talking with the
            // initial caller (audio analysis?). For now, we assume that the
            // transfer succeeded if the call is active for the caller,
            // dropped for the callee and active for the destination.
            setTimeout(function() {
                expect(phoneCall.terminated).toBe(true);
                expect(phoneCall2.terminated).toBe(false);
                expect(phoneCall3.terminated).toBe(false);
                testStatus = 'transferred call terminated by receiver';
                phoneCall3.terminate();
            }, TIMEOUT_ASSUME_ANSWER_COMPLETED);
        });

        function startTest() {
            phoneCall = myphone.startCall(getNumber(USER_CALLEE));
            pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();
            // The test is not finished yet when phoneCall ends. After the call
            // transfer to myphone3, myphone2 and myphone3 should be talking,
            // and then the completion of the test is detected by phoneCall2
            // getting dropped.
            onEndedOrDestroyed(myphone, phoneCall, function() {
                if (!phoneCall2) {
                    checkExpectations();
                }
            });

            phoneCall.on('answered', function() {
                expect(phoneCall.answered).toBe(true);
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall2.answered).toBe(true);
                expect(phoneCall2.terminated).toBe(false);
                expect(phoneCall3).toBeUndefined();

                // Verify that call_id are equal before transferring. After a
                // transfer, the call_id will change.
                expect(phoneCall.call_id).toEqual(phoneCall2.call_id);

                // myphone and myphone2 are now talking. Let's transfer the call
                // from myphone2 to myphone3.
                testStatus = 'will call transferCall';
                phoneCall.transferCall(getNumber(USER_CALLEE2));
                testStatus = 'called transferCall';
            });
        }

        function checkExpectations() {
            expect(testStatus).toBe('transferred call terminated by receiver');

            expect(getCalls(pSpy)).toEqual(['connected', 'created']);
            expect(getCalls(pSpy2)).toEqual(['connected', 'created',
                    'incoming']);
            expect(getCalls(pSpy3)).toEqual(['connected', 'created',
                    'incoming']);

            expect(getCalls(pcSpy)).toEqual(['initialized', 'outgoing_progress',
                    'answered', 'ended']);
            expect(getCalls(pcSpy2)).toEqual(['initialized', 'answered',
                    'call_id_changed', 'ended']);
            expect(getCalls(pcSpy3)).toEqual(['initialized', 'answered',
                    'call_id_changed', 'ended']);

            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall.answered).toBe(true);

            if (phoneCall2) {
                expect(phoneCall2.terminated).toBe(true);
                expect(phoneCall2.answered).toBe(true);
            }

            if (phoneCall3) {
                expect(phoneCall3.terminated).toBe(true);
                expect(phoneCall3.answered).toBe(true);
            }

            if (!phoneCall2 || !phoneCall3) {
                done();
                return;
            }

            // call_id_changed, so should not be equal:
            expect(phoneCall.call_id).not.toEqual(phoneCall2.call_id);
            expect(phoneCall.my_leg_uuid).toEqual(phoneCall2.other_leg_uuid);
            expect(phoneCall.other_leg_uuid).not.toEqual(
                    phoneCall2.my_leg_uuid);

            expect(phoneCall3.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall.my_leg_uuid);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall2.my_leg_uuid);
            expect(phoneCall3.other_leg_uuid).toEqual(phoneCall2.my_leg_uuid);
            done();
        }
    }, TIMEOUT_CONNECT + 2 * TIMEOUT_BRIDGE + TIMEOUT_ALWAYS);

    // Disabled because the current implementation does not allow this. Perhaps
    // it becomes possible after upgrading JsSIP.
    xit('should allow call transfer of incoming call', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var myphone2 = createPhone(getPhoneConfig(USER_CALLEE));
        var pSpy2 = spyOn(myphone2, 'trigger').and.callThrough();

        var myphone3 = createPhone(getPhoneConfig(USER_CALLEE2));
        var pSpy3 = spyOn(myphone3, 'trigger').and.callThrough();

        var pcSpy;
        var pcSpy2;
        var pcSpy3;
        var phoneCall;
        var phoneCall2;
        var phoneCall3;
        var testStatus = 'did not call transferCall yet!';

        Promise.all([
            promiseRegistration(myphone),
            promiseRegistration(myphone2),
            promiseRegistration(myphone3),
        ]).then(startTest);

        myphone2.once('incoming', function(event) {
            phoneCall2 = event.phoneCall;
            pcSpy2 = spyOn(phoneCall2, 'trigger').and.callThrough();

            // Implementation detail: Cannot call transfer before the call is
            // initialized.
            expect(function() {
                testStatus = 'will call transferCall1';
                phoneCall2.transferCall(getNumber(USER_CALLEE2));
                testStatus = 'called transferCall2';
            }).toThrow();

            expect(phoneCall2.rtcSession).toBeNull();
            phoneCall2.once('initialized', function() {
                testStatus = 'will call transferCall';
                phoneCall2.transferCall(getNumber(USER_CALLEE2));
                testStatus = 'called transferCall';
            });
        });

        myphone3.once('incoming', function(event) {
            phoneCall3 = event.phoneCall;
            pcSpy3 = spyOn(phoneCall3, 'trigger').and.callThrough();

            expect(phoneCall2.answered).toBe(false);
            expect(phoneCall2.terminated).toBe(true);

            expect(phoneCall3.terminated).toBe(false);
            expect(phoneCall3.incoming).toBe(true);
            expect(phoneCall3.answered).toBe(false);

            phoneCall3.answer();

            // TODO: Somehow check whether we are really talking with the
            // initial caller (audio analysis?). For now, we assume that the
            // transfer succeeded if the call is active for the caller,
            // dropped for the callee and active for the destination.
            setTimeout(function() {
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall2.terminated).toBe(true);
                expect(phoneCall3.terminated).toBe(false);
                testStatus = 'transferred call terminated by receiver';
                phoneCall3.terminate();
            }, TIMEOUT_ASSUME_ANSWER_COMPLETED);
        });

        function startTest() {
            phoneCall = myphone.startCall(getNumber(USER_CALLEE));
            pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();
            onEndedOrDestroyed(myphone, phoneCall, checkExpectations);

            phoneCall.on('answered', function() {
                expect(phoneCall.answered).toBe(true);
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall2.answered).toBe(true);
                expect(phoneCall2.terminated).toBe(false);
                expect(phoneCall3).toBeUndefined();
            });
        }

        function checkExpectations() {
            expect(testStatus).toBe('transferred call terminated by receiver');

            expect(getCalls(pSpy)).toEqual(['connected', 'created']);
            expect(getCalls(pSpy2)).toEqual(['connected', 'created',
                    'incoming']);
            expect(getCalls(pSpy3)).toEqual(['connected', 'created',
                    'incoming']);

            expect(getCalls(pcSpy)).toEqual(['initialized', 'outgoing_progress',
                    'answered', 'ended']);
            expect(getCalls(pcSpy2)).toEqual(['initialized', 'answered',
                    'ended']);
            expect(getCalls(pcSpy3)).toEqual(['initialized', 'answered',
                    'ended']);

            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall.answered).toBe(true);

            if (phoneCall2) {
                expect(phoneCall2.terminated).toBe(true);
                expect(phoneCall2.answered).toBe(true);
            }

            if (phoneCall3) {
                expect(phoneCall3.terminated).toBe(true);
                expect(phoneCall3.answered).toBe(true);
            }

            if (!phoneCall2 || !phoneCall3) {
                done();
                return;
            }

            expect(phoneCall.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall.my_leg_uuid).toEqual(phoneCall2.other_leg_uuid);
            expect(phoneCall.other_leg_uuid).toEqual(phoneCall2.my_leg_uuid);

            expect(phoneCall3.call_id).toEqual(phoneCall2.call_id);
            // Note: Currently we let the destination take over the leg UUID of
            // the original callee. This may change in the future, when we add
            // support for multiple participants per call.
            expect(phoneCall3.my_leg_uuid).toEqual(phoneCall2.my_leg_uuid);
            expect(phoneCall3.other_leg_uuid).toEqual(
                    phoneCall2.other_leg_uuid);

            done();
        }
    }, TIMEOUT_CONNECT + 2 * TIMEOUT_BRIDGE + TIMEOUT_ALWAYS);

    it('should support attended transfer for incoming call', function(done) {
        // myphone will dial myphone2, which answers the call.
        // myphone2 calls myphone 3 and transfer myphone to myphone3.
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var myphone2 = createPhone(getPhoneConfig(USER_CALLEE));
        var pSpy2 = spyOn(myphone2, 'trigger').and.callThrough();

        var myphone3 = createPhone(getPhoneConfig(USER_CALLEE2));
        var pSpy3 = spyOn(myphone3, 'trigger').and.callThrough();

        var pcSpy;
        var pcSpy2;
        var pcSpy2to3;
        var pcSpy3;
        var phoneCall;
        var phoneCall2;
        var phoneCall2to3;
        var phoneCall3;
        var testStatus = 'did not call transferCall yet!';

        Promise.all([
            promiseRegistration(myphone),
            promiseRegistration(myphone2),
            promiseRegistration(myphone3),
        ]).then(startTest);

        myphone2.once('incoming', function(event) {
            phoneCall2 = event.phoneCall;
            pcSpy2 = spyOn(phoneCall2, 'trigger').and.callThrough();

            phoneCall2.answer();
        });

        myphone3.once('incoming', function(event) {
            phoneCall3 = event.phoneCall;
            pcSpy3 = spyOn(phoneCall3, 'trigger').and.callThrough();

            expect(phoneCall3.terminated).toBe(false);
            expect(phoneCall3.incoming).toBe(true);
            expect(phoneCall3.answered).toBe(false);

            phoneCall3.answer();
            // Test will continue in assume1_connected_to_3.
        });

        function assume1_connected_to_3() {
            // TODO: Somehow check whether we are really talking with the
            // initial caller (audio analysis?). For now, we assume that the
            // transfer succeeded if the call is active for the caller,
            // dropped for the callee and active for the destination.
            expect(phoneCall.terminated).toBe(false);
            expect(phoneCall2.terminated).toBe(true);
            expect(phoneCall2to3.terminated).toBe(true);
            expect(phoneCall3.terminated).toBe(false);
            testStatus = 'transferred call terminated by receiver';
            phoneCall3.terminate();
        }

        function startTest() {
            phoneCall = myphone.startCall(getNumber(USER_CALLEE));
            pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();
            onEndedOrDestroyed(myphone, phoneCall, checkExpectations);

            phoneCall.on('answered', function() {
                expect(phoneCall.answered).toBe(true);
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall2.answered).toBe(true);
                expect(phoneCall2.terminated).toBe(false);
                expect(phoneCall2to3).toBeUndefined();
                expect(phoneCall3).toBeUndefined();

                // myphone and myphone2 are now talking. Let's transfer the call
                // from myphone2 to myphone3.
                testStatus = 'will create receiver for transferCall';
                phoneCall2to3 = myphone2.startCall(getNumber(USER_CALLEE2));
                pcSpy2to3 = spyOn(phoneCall2to3, 'trigger').and.callThrough();
                phoneCall2to3.once('answered', function() {
                    testStatus = 'will call transferCall';
                    // The destination will be set if specified, otherwise the
                    // destination is automatically derived from the PhoneCall.
                    phoneCall2.transferCall('', phoneCall2to3);
                    testStatus = 'called transferCall';
                    setTimeout(function() {
                        assume1_connected_to_3();
                    }, TIMEOUT_ASSUME_ANSWER_COMPLETED);
                });
            });
        }

        function checkExpectations() {
            expect(testStatus).toBe('transferred call terminated by receiver');

            expect(getCalls(pSpy)).toEqual(['connected', 'created']);
            expect(getCalls(pSpy2)).toEqual(['connected', 'created',
                    'incoming', 'created']);
            expect(getCalls(pSpy3)).toEqual(['connected', 'created',
                    'incoming']);

            expect(getCalls(pcSpy)).toEqual(['initialized', 'outgoing_progress',
                    'answered', 'ended']);
            expect(getCalls(pcSpy2)).toEqual(['initialized', 'answered',
                    'ended']);
            expect(getCalls(pcSpy2to3)).toEqual(['initialized',
                    'outgoing_progress', 'answered', 'ended']);
            expect(getCalls(pcSpy3)).toEqual(['initialized', 'answered',
                    'call_id_changed', 'ended']);

            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall.answered).toBe(true);

            if (phoneCall2) {
                expect(phoneCall2.terminated).toBe(true);
                expect(phoneCall2.answered).toBe(true);
            }

            if (phoneCall2to3) {
                expect(phoneCall2to3.terminated).toBe(true);
                expect(phoneCall2to3.answered).toBe(true);
            }


            if (phoneCall3) {
                expect(phoneCall3.terminated).toBe(true);
                expect(phoneCall3.answered).toBe(true);
            }

            if (!phoneCall2 || !phoneCall2to3 || !phoneCall3) {
                done();
                return;
            }

            expect(phoneCall.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall.my_leg_uuid).toEqual(phoneCall2.other_leg_uuid);
            expect(phoneCall.other_leg_uuid).not.toEqual(
                    phoneCall2.my_leg_uuid);

            // phoneCall2to3 is a new call, but after the transfer the call_id
            // is changed to the call_id of the original call.
            expect(phoneCall2to3.call_id).not.toEqual(phoneCall2.call_id);
            expect(phoneCall2to3.call_id).not.toEqual(phoneCall3.call_id);
            expect(phoneCall2to3.my_leg_uuid).not.toEqual(
                   phoneCall2.my_leg_uuid);

            expect(phoneCall3.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall.my_leg_uuid);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall2.my_leg_uuid);
            expect(phoneCall3.my_leg_uuid).not.toEqual(
                    phoneCall2to3.my_leg_uuid);
            expect(phoneCall3.other_leg_uuid).toEqual(
                    phoneCall2to3.my_leg_uuid);

            done();
        }
    }, TIMEOUT_CONNECT + 2 * TIMEOUT_BRIDGE + TIMEOUT_ALWAYS);

    it('should support transfer of incoming call to new call', function(done) {
        // myphone will dial myphone2, which answers the call.
        // myphone2 calls myphone 3 and transfer myphone to myphone3 without
        // waiting for the call to be set up.
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var myphone2 = createPhone(getPhoneConfig(USER_CALLEE));
        var pSpy2 = spyOn(myphone2, 'trigger').and.callThrough();

        var myphone3 = createPhone(getPhoneConfig(USER_CALLEE2));
        var pSpy3 = spyOn(myphone3, 'trigger').and.callThrough();

        var pcSpy;
        var pcSpy2;
        var pcSpy2to3;
        var pcSpy3;
        var phoneCall;
        var phoneCall2;
        var phoneCall2to3;
        var phoneCall3;
        var testStatus = 'did not call transferCall yet!';

        Promise.all([
            promiseRegistration(myphone),
            promiseRegistration(myphone2),
            promiseRegistration(myphone3),
        ]).then(startTest);

        myphone2.once('incoming', function(event) {
            phoneCall2 = event.phoneCall;
            pcSpy2 = spyOn(phoneCall2, 'trigger').and.callThrough();

            phoneCall2.answer();
        });

        myphone3.once('incoming', function(event) {
            phoneCall3 = event.phoneCall;
            pcSpy3 = spyOn(phoneCall3, 'trigger').and.callThrough();

            expect(phoneCall3.terminated).toBe(false);
            expect(phoneCall3.incoming).toBe(true);
            expect(phoneCall3.answered).toBe(false);

            phoneCall3.answer();
            setTimeout(function() {
                assume1_connected_to_3();
            }, TIMEOUT_ASSUME_ANSWER_COMPLETED);
        });

        function assume1_connected_to_3() {
            // TODO: Somehow check whether we are really talking with the
            // initial caller (audio analysis?). For now, we assume that the
            // transfer succeeded if the call is active for the caller,
            // dropped for the callee and active for the destination.
            expect(phoneCall.terminated).toBe(false);
            expect(phoneCall2.terminated).toBe(true);
            expect(phoneCall2to3.terminated).toBe(true);
            expect(phoneCall3.terminated).toBe(false);
            testStatus = 'transferred call terminated by receiver';
            phoneCall3.terminate();
        }

        function startTest() {
            phoneCall = myphone.startCall(getNumber(USER_CALLEE));
            pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();
            onEndedOrDestroyed(myphone, phoneCall, checkExpectations);

            phoneCall.on('answered', function() {
                expect(phoneCall.answered).toBe(true);
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall2.answered).toBe(true);
                expect(phoneCall2.terminated).toBe(false);
                expect(phoneCall2to3).toBeUndefined();
                expect(phoneCall3).toBeUndefined();

                // myphone and myphone2 are now talking. Let's transfer the call
                // from myphone2 to myphone3.
                testStatus = 'will create receiver for transferCall';
                phoneCall2to3 = myphone2.startCall(getNumber(USER_CALLEE2));
                pcSpy2to3 = spyOn(phoneCall2to3, 'trigger').and.callThrough();
                testStatus = 'will call transferCall';
                // The destination will be set if specified, otherwise the
                // destination is automatically derived from the PhoneCall.
                phoneCall2.transferCall('', phoneCall2to3);
                testStatus = 'called transferCall';
            });
        }

        function checkExpectations() {
            expect(testStatus).toBe('transferred call terminated by receiver');

            expect(getCalls(pSpy)).toEqual(['connected', 'created']);
            expect(getCalls(pSpy2)).toEqual(['connected', 'created',
                    'incoming', 'created']);
            expect(getCalls(pSpy3)).toEqual(['connected', 'created',
                    'incoming']);

            expect(getCalls(pcSpy)).toEqual(['initialized', 'outgoing_progress',
                    'answered', 'ended']);
            expect(getCalls(pcSpy2)).toEqual(['initialized', 'answered',
                    'ended']);
            expect(getCalls(pcSpy2to3)).toEqual(['initialized',
                    'outgoing_progress', 'ended']);
            expect(getCalls(pcSpy3)).toEqual(['initialized', 'answered',
                    'call_id_changed', 'ended']);

            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall.answered).toBe(true);

            if (phoneCall2) {
                expect(phoneCall2.terminated).toBe(true);
                expect(phoneCall2.answered).toBe(true);
            }

            if (phoneCall2to3) {
                expect(phoneCall2to3.terminated).toBe(true);
                expect(phoneCall2to3.answered).toBe(false);
            }


            if (phoneCall3) {
                expect(phoneCall3.terminated).toBe(true);
                expect(phoneCall3.answered).toBe(true);
            }

            if (!phoneCall2 || !phoneCall2to3 || !phoneCall3) {
                done();
                return;
            }

            expect(phoneCall.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall.my_leg_uuid).toEqual(phoneCall2.other_leg_uuid);
            expect(phoneCall.other_leg_uuid).not.toEqual(
                    phoneCall2.my_leg_uuid);

            // phoneCall2to3 is a new call, but after the transfer the call_id
            // is changed to the call_id of the original call.
            expect(phoneCall2to3.call_id).not.toEqual(phoneCall2.call_id);
            expect(phoneCall2to3.call_id).not.toEqual(phoneCall3.call_id);
            expect(phoneCall2to3.my_leg_uuid).not.toEqual(
                   phoneCall2.my_leg_uuid);

            expect(phoneCall3.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall.my_leg_uuid);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall2.my_leg_uuid);
            expect(phoneCall3.my_leg_uuid).not.toEqual(
                    phoneCall2to3.my_leg_uuid);
            expect(phoneCall3.other_leg_uuid).toEqual(
                    phoneCall2to3.my_leg_uuid);

            done();
        }
    }, TIMEOUT_CONNECT + 2 * TIMEOUT_BRIDGE + TIMEOUT_ALWAYS);

    it('should support attended transfer for outgoing call', function(done) {
        // myphone will dial myphone2 and myphone3, both will answer the call.
        // Then myphone transfers the call and lets 2 talk to 3.
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();

        var myphone2 = createPhone(getPhoneConfig(USER_CALLEE));
        var pSpy2 = spyOn(myphone2, 'trigger').and.callThrough();

        var myphone3 = createPhone(getPhoneConfig(USER_CALLEE2));
        var pSpy3 = spyOn(myphone3, 'trigger').and.callThrough();

        var pcSpy;
        var pcSpy1to3;
        var pcSpy2;
        var pcSpy3;
        var phoneCall;
        var phoneCall1to3;
        var phoneCall2;
        var phoneCall3;
        var testStatus = 'did not call transferCall yet!';

        Promise.all([
            promiseRegistration(myphone),
            promiseRegistration(myphone2),
            promiseRegistration(myphone3),
        ]).then(startTest);

        myphone2.once('incoming', function(event) {
            phoneCall2 = event.phoneCall;
            pcSpy2 = spyOn(phoneCall2, 'trigger').and.callThrough();

            phoneCall2.answer();
        });

        myphone3.once('incoming', function(event) {
            phoneCall3 = event.phoneCall;
            pcSpy3 = spyOn(phoneCall3, 'trigger').and.callThrough();

            phoneCall3.answer();
        });

        function assume2_connected_to_3() {
            // TODO: Somehow check whether we are really talking with the
            // initial caller (audio analysis?). For now, we assume that the
            // transfer succeeded if the call is active for the caller,
            // dropped for the callee and active for the destination.
            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall1to3.terminated).toBe(true);
            expect(phoneCall2.terminated).toBe(false);
            expect(phoneCall3.terminated).toBe(false);
            testStatus = 'transferred call terminated by receiver';
            phoneCall3.terminate();
        }

        function startTest() {
            phoneCall = myphone.startCall(getNumber(USER_CALLEE));
            pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

            phoneCall1to3 = myphone.startCall(getNumber(USER_CALLEE2));
            pcSpy1to3 = spyOn(phoneCall1to3, 'trigger').and.callThrough();

            var pc2_and3_answered = false;
            onEndedOrDestroyed(myphone, phoneCall, function() {
                if (!pc2_and3_answered) {
                    // phoneCall3 is responsible for checking. But if phone3 is
                    // never reached and the test times out, check anyway.
                    checkExpectations();
                }
            });

            Promise.all([
                new Promise(function(resolve) {
                    phoneCall.once('answered', resolve);
                }),
                new Promise(function(resolve) {
                    phoneCall1to3.once('answered', resolve);
                })
            ]).then(function() {
                expect(phoneCall.answered).toBe(true);
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall1to3.answered).toBe(true);
                expect(phoneCall1to3.terminated).toBe(false);
                expect(phoneCall2.answered).toBe(true);
                expect(phoneCall2.terminated).toBe(false);
                expect(phoneCall3.answered).toBe(true);
                expect(phoneCall3.terminated).toBe(false);
                pc2_and3_answered = true;
                onEndedOrDestroyed(myphone2, phoneCall2, checkExpectations);

                testStatus = 'will call transferCall';
                // Attended transfer appears to work even if the receiver
                // did not accept the call yet.
                // The destination will be set if specified, otherwise the
                // destination is automatically derived from the PhoneCall.
                phoneCall.transferCall('', phoneCall1to3);
                testStatus = 'called transferCall';
                setTimeout(function() {
                    assume2_connected_to_3();
                }, TIMEOUT_ASSUME_ANSWER_COMPLETED);
            });
        }

        function checkExpectations() {
            expect(testStatus).toBe('transferred call terminated by receiver');

            expect(getCalls(pSpy)).toEqual(['connected', 'created', 'created']);
            expect(getCalls(pSpy2)).toEqual(['connected', 'created',
                    'incoming']);
            expect(getCalls(pSpy3)).toEqual(['connected', 'created',
                    'incoming']);

            expect(getCalls(pcSpy)).toEqual(['initialized', 'outgoing_progress',
                    'answered', 'ended']);
            expect(getCalls(pcSpy2)).toEqual(['initialized', 'answered',
                    'call_id_changed', 'ended']);
            expect(getCalls(pcSpy1to3)).toEqual(['initialized',
                    'outgoing_progress', 'answered', 'ended']);
            expect(getCalls(pcSpy3)).toEqual(['initialized', 'answered',
                    'call_id_changed', 'ended']);

            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall.answered).toBe(true);

            if (phoneCall2) {
                expect(phoneCall2.terminated).toBe(true);
                expect(phoneCall2.answered).toBe(true);
            }

            if (phoneCall1to3) {
                expect(phoneCall1to3.terminated).toBe(true);
                expect(phoneCall1to3.answered).toBe(true);
            }


            if (phoneCall3) {
                expect(phoneCall3.terminated).toBe(true);
                expect(phoneCall3.answered).toBe(true);
            }

            if (!phoneCall2 || !phoneCall1to3 || !phoneCall3) {
                done();
                return;
            }

            expect(phoneCall.call_id).not.toEqual(phoneCall1to3.call_id);
            // After call transfer, all receipients will change call_id
            expect(phoneCall.call_id).not.toEqual(phoneCall2.call_id);
            expect(phoneCall.call_id).not.toEqual(phoneCall3.call_id);
            expect(phoneCall.my_leg_uuid).toEqual(phoneCall2.other_leg_uuid);
            expect(phoneCall.other_leg_uuid).toEqual('');

            expect(phoneCall1to3.call_id).not.toEqual(phoneCall2.call_id);
            expect(phoneCall1to3.call_id).not.toEqual(phoneCall3.call_id);
            expect(phoneCall1to3.my_leg_uuid).toEqual(
                    phoneCall3.other_leg_uuid);
            expect(phoneCall1to3.other_leg_uuid).toEqual('');

            expect(phoneCall2.other_leg_uuid).toEqual(phoneCall.my_leg_uuid);

            expect(phoneCall3.call_id).toEqual(phoneCall2.call_id);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall.my_leg_uuid);
            expect(phoneCall3.my_leg_uuid).not.toEqual(phoneCall2.my_leg_uuid);
            expect(phoneCall3.my_leg_uuid).not.toEqual(
                    phoneCall1to3.my_leg_uuid);

            done();
        }
    }, TIMEOUT_CONNECT + 2 * TIMEOUT_BRIDGE + TIMEOUT_ALWAYS);

    it('should be able to eavesdrop on call', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var pSpy = spyOn(myphone, 'trigger').and.callThrough();
        var myphone2 = createPhone(getPhoneConfig(USER_CALLEE));
        var pSpy2 = spyOn(myphone2, 'trigger').and.callThrough();
        var myphone3 = createPhone(getPhoneConfig(USER_EAVESDROPPER));
        var pSpy3 = spyOn(myphone3, 'trigger').and.callThrough();

        var pcSpy;
        var pcSpy2;
        var pcSpy3;
        var pcSpy4;
        var phoneCall;
        var phoneCall2;
        var phoneCall3;
        var phoneCall4;

        Promise.all([
            promiseRegistration(myphone),
            promiseRegistration(myphone2),
            promiseRegistration(myphone3),
        ]).then(startTest);

        myphone2.once('incoming', function(event) {
            phoneCall2 = event.phoneCall;
            pcSpy2 = spyOn(phoneCall2, 'trigger').and.callThrough();
            phoneCall2.answer();
        });


        function startTest() {
            phoneCall = myphone.startCall(getNumber(USER_CALLEE));
            pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();
            onEndedOrDestroyed(myphone, phoneCall, checkExpectations);
            phoneCall.on('answered', function() {
                expect(phoneCall.answered).toBe(true);
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall2.answered).toBe(true);
                expect(phoneCall2.terminated).toBe(false);

                startEavesdroppingAndFinish();
            });
        }

        function startEavesdroppingAndFinish() {
            expect(phoneCall3).toBeUndefined();
            phoneCall3 = myphone3.listenInCall(getNumber(USER_CALLER));
            pcSpy3 = spyOn(phoneCall3, 'trigger').and.callThrough();

            expect(phoneCall4).toBeUndefined();
            phoneCall4 = myphone3.listenInCall(getNumber(USER_CALLEE));
            pcSpy4 = spyOn(phoneCall4, 'trigger').and.callThrough();

            Promise.all([
                new Promise(function(resolve) {
                    phoneCall3.on('answered', resolve);
                    phoneCall3.on('ended', resolve);
                }),
                new Promise(function(resolve) {
                    phoneCall4.on('answered', resolve);
                    phoneCall4.on('ended', resolve);
                })
            ]).then(function() {
                // Terminate call. This should trigger checkExpectations.
                phoneCall2.terminate();
            });
        }

        function checkExpectations() {
            // When the call has ended, the "calls" of eavesdroppers should also
            // be dropped.
            Promise.all([
                new Promise(function(resolve) {
                    setTimeout(resolve, TIMEOUT_ASSUME_HANGUP_PROPAGATED);
                    if (phoneCall3) {
                        if (phoneCall3.terminated) resolve();
                        else phoneCall3.on('ended', resolve);
                    }
                }),
                new Promise(function(resolve) {
                    setTimeout(resolve, TIMEOUT_ASSUME_HANGUP_PROPAGATED);
                    if (phoneCall4) {
                        if (phoneCall4.terminated) resolve();
                        else phoneCall4.on('ended', resolve);
                    }
                })
            ]).then(checkExpectationsForReal);
        }
        function checkExpectationsForReal() {
            expect(getCalls(pSpy)).toEqual(['connected', 'created']);
            expect(getCalls(pSpy2)).toEqual(['connected', 'created',
                    'incoming']);
            expect(getCalls(pSpy3)).toEqual(['connected', 'created',
                    'created']);

            expect(getCalls(pcSpy)).toEqual(['initialized', 'outgoing_progress',
                    'answered', 'ended']);
            expect(getCalls(pcSpy2)).toEqual(['initialized', 'answered',
                    'ended']);
            expect(getCalls(pcSpy3)).toEqual(['initialized', 'answered',
                    'ended']);
            expect(getCalls(pcSpy4)).toEqual(['initialized', 'answered',
                    'ended']);

            if (!phoneCall || !phoneCall2 || !phoneCall3 || !phoneCall4) {
                done();
                return;
            }

            expect(phoneCall.terminated).toBe(true);
            expect(phoneCall2.terminated).toBe(true);
            expect(phoneCall3.terminated).toBe(true);
            expect(phoneCall4.terminated).toBe(true);
            expect(phoneCall.answered).toBe(true);
            expect(phoneCall2.answered).toBe(true);
            expect(phoneCall3.answered).toBe(true);
            expect(phoneCall4.answered).toBe(true);

            done();
        }
    }, TIMEOUT_CONNECT + 2 * TIMEOUT_BRIDGE + TIMEOUT_ALWAYS);

    it('should support hold / unhold / isOnHold during call', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var phoneCall = myphone.startCall(getNumber(USER_AUTO_ANSWER));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        phoneCall.once('answered', function() {
            expect(phoneCall.isOnHold()).toBe(false);
            expect(phoneCall.isReallyOnHold()).toBe(false);
            expect(getCalls(pcSpy)).not.toContain('holdchanged');
            expect(function() {
                phoneCall.hold();
            }).not.toThrow();
            expect(phoneCall.isOnHold()).toBe(true);
            // Putting on hold can be done without waiting for the other end,
            // simply disable the media.
            expect(phoneCall.isReallyOnHold()).toBe(true);
            expect(getCalls(pcSpy)).toContain('holdchanged');

            expect(function() {
                phoneCall.unhold();
            }).not.toThrow();
            // Unholding does not immediately re-activate the hold state,
            // because the media has to be renegotiated with the server.
            //
            // TODO: After upgrading from JsSIP 1.0.0 to 3.3.2+, isReallyOnHold
            // started to return false instead of true in this test. Investigate
            // whether this is correct.
            expect(phoneCall.isReallyOnHold()).toBe(false);
            expect(phoneCall.isOnHold()).toBe(false);

            new Promise(function(resolve) {
                phoneCall.once('holdchanged', resolve);
                setTimeout(resolve, TIMEOUT_HOLD_CHANGE);
            }).then(function() {
                // hold / unhold should not cause call termination.
                expect(phoneCall.terminated).toBe(false);
                expect(phoneCall.isReallyOnHold()).toBe(false);
                expect(phoneCall.isOnHold()).toBe(false);
                phoneCall.terminate();
            });
        });
        onEndedOrDestroyed(myphone, phoneCall, function() {
            expect(getCalls(pcSpy)).toEqual(['initialized', 'answered',
                    'holdchanged', 'holdchanged', 'ended']);
            expect(phoneCall.isOnHold()).toBe(false);
            done();
        });
    }, TIMEOUT_CONNECT_AND_ANSWER + TIMEOUT_HOLD_CHANGE + TIMEOUT_ALWAYS);

    it('should support hold / unhold / isOnHold before call', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var phoneCall = myphone.startCall(getNumber(USER_AUTO_ANSWER));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();

        phoneCall.once('answered', function() {
            expect(phoneCall.isOnHold()).toBe(true);
            // The hold request has just been sent, it's unlikely that it is
            // already processed...
            expect(phoneCall.isReallyOnHold()).toBe(false);
        });
        phoneCall.once('holdchanged', function() {
            expect(phoneCall.isReallyOnHold()).toBe(true);
            phoneCall.terminate();
        });
        onEndedOrDestroyed(myphone, phoneCall, function() {
            expect(getCalls(pcSpy)).toEqual(['initialized', 'answered',
                    'holdchanged', 'ended']);
            expect(phoneCall.isReallyOnHold()).toBe(true);
            expect(phoneCall.isOnHold()).toBe(true);
            done();
        });

        expect(phoneCall.isOnHold()).toBe(false);
        expect(function() {
            phoneCall.unhold();
        }).not.toThrow();
        expect(phoneCall.isOnHold()).toBe(false);
        expect(function() {
            phoneCall.hold();
        }).not.toThrow();
        expect(phoneCall.isOnHold()).toBe(true);
    }, TIMEOUT_CONNECT_AND_ANSWER + TIMEOUT_ALWAYS);

    it('should support debugEnabled = false (default)', function(done) {
        expect(phone.debugEnabled).toBe(false);

        var consoleMessages = [];
        spyOn(console, 'log').and.callFake(interceptLog);
        spyOn(console, 'error').and.callFake(interceptLog);
        spyOn(console, 'warn').and.callFake(interceptLog);
        spyOn(console, 'debug').and.callFake(interceptLog);
        spyOn(console, 'info').and.callFake(interceptLog);
        function interceptLog() {
            consoleMessages.push.apply(consoleMessages, arguments);
        }

        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var phoneCall = myphone.startCall(getNumber(USER_AUTO_ANSWER));
        phoneCall.once('answered', function() {
            phoneCall.terminate();
        });

        onEndedOrDestroyed(myphone, phoneCall, function() {
            phone.debugEnabled = false;
            expect(phone.debugEnabled).toBe(false);

            // Config (initialization).
            expect(consoleMessages).not.toMatch(/WebSocketInterface.+wss:/);

            // SIP registration, invitation and termination.
            expect(consoleMessages).not.toMatch(/REGISTER sip:/);
            expect(consoleMessages).not.toMatch(/INVITE sip:/);
            expect(consoleMessages).not.toMatch(/BYE sip:/);

            // ICE
            expect(consoleMessages).not.toMatch(/ typ host /);

            // SDP
            expect(consoleMessages).not.toMatch(/a=candidate:/);
            done();
        });
    }, TIMEOUT_CONNECT_AND_ANSWER + TIMEOUT_ALWAYS);

    it('should support debugEnabled = true', function(done) {
        expect(phone.debugEnabled).toBe(false);
        phone.debugEnabled = true;
        expect(phone.debugEnabled).toBe(true);

        // Spammy messages goes to console.log,
        // single-line messages ought to go to console.error for the stack
        // trace.
        var consoleSpy = spyOn(console, 'log').and.callThrough();
        var consoleErrSpy = spyOn(console, 'error').and.callThrough();

        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var phoneCall = myphone.startCall(getNumber(USER_AUTO_ANSWER));
        phoneCall.once('answered', function() {
            phoneCall.terminate();
        });

        onEndedOrDestroyed(myphone, phoneCall, function() {
            phone.debugEnabled = false;
            expect(phone.debugEnabled).toBe(false);

            // Config (initialization).
            expect(consoleErrSpy.calls.allArgs()).toMatch(
                /WebSocketInterface.+wss:/);

            // SIP registration, invitation and termination.
            expect(consoleSpy.calls.allArgs()).toMatch(/REGISTER sip:/);
            expect(consoleSpy.calls.allArgs()).toMatch(/INVITE sip:/);
            expect(consoleSpy.calls.allArgs()).toMatch(/BYE sip:/);

            // ICE
            expect(consoleSpy.calls.allArgs()).toMatch(/ typ host /);

            // SDP
            expect(consoleSpy.calls.allArgs()).toMatch(/a=candidate:/);
            done();
        });
    }, TIMEOUT_CONNECT_AND_ANSWER + TIMEOUT_ALWAYS);

    it('should support sendKey', function(done) {
        var myphone = createPhone(getPhoneConfig(USER_CALLER));
        var phoneCall = myphone.startCall(getNumber(USER_AUTO_ANSWER));
        var pcSpy = spyOn(phoneCall, 'trigger').and.callThrough();
        phoneCall.once('answered', function() {
            pcSpy.calls.reset();
            expect(function() {
                phoneCall.sendKey('123456798ABCD#*');
            }).not.toThrow();
            phoneCall.terminate();
        });

        onEndedOrDestroyed(myphone, phoneCall, function() {
            expect(getCalls(pcSpy)).toEqual(['dtmf', 'ended']);
            expect(pcSpy.calls.argsFor(0)).toBeEvent({
                type: 'dtmf',
                dtmf: '1',
                isRemote: false,
                phoneCall: phoneCall,
            });
            done();
        });
    }, TIMEOUT_CONNECT_AND_ANSWER + TIMEOUT_ALWAYS);

    // TODO: Test Phone listenInCall for user without eavesdropping rights.
    // TODO: Test sendKey and check whether the tone was received.
    // TODO: Test wrappedGetUserMedia
    // TODO: Test media and volume
    // TODO: Test PhoneSounds.
});
