Depends on JsSIP - https://github.com/versatica/JsSIP.

A copy of JsSIP is added to this repository:

```
git clone https://github.com/versatica/JsSIP.git
cd JsSIP
npm install
./node_modules/.bin/gulp
cp dist/jssip.js ..
```

`build.js` generates the result in `dist/phone.js`.

The API is documented in jsdoc style. You can auto-generate the documentation
files using any jsdoc generation tool, such as jsdoc or docker (install via npm).

This is the client library. Some functionality depends on the backend. This logic
can be found in a separate repository at https://bitbucket.org/codeyellow/bellen.
Look in the dialplan/ directory for the "server-side" logic.

## Testing with Chrome

Recent versions of Chrome have restricted microphone (and camera access) to
secure origins and localhost. If you use http, either run from localhost,
or start Chrome with the `--unsafely-treat-insecure-origin-as-secure` flag.
For example, if you are hosting call functionality from `http://10.10.10.71`, use:

    chromium --user-data-dir=/tmp/whatever --unsafely-treat-insecure-origin-as-secure=http://10.10.10.71 http://10.10.10.71
