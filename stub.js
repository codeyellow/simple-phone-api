/**
 * Mock the API for unit tests.
 * All methods are non-functional, except for the Event emitter API.
 */
define(['backbone'], function(Backbone) {
    'use strict';
    function declareMethod(obj, methodName) {
        obj[methodName] = function noop() {
            if (typeof console === 'object' && console && console.log) {
                console.log('Note: ' + methodName + ' not implemented');
            }
        };
    }
    function declareProtoMethod(obj, methodName) {
        declareMethod(obj.prototype, methodName);
    }

    function Phone(options) {
        this.options = options;
        // In reality, it should be non-null when the phone is ready...
        this.jssipPhone = null;
        this.phoneCalls = [];
    }
    Phone.prototype = Object.create(Backbone.Events);
    Phone.prototype.constructor = Phone;
    declareProtoMethod(Phone, 'startCall');
    declareProtoMethod(Phone, 'listenInCall');
    declareProtoMethod(Phone, 'destroy');
    declareProtoMethod(Phone, 'logDebug');

    function PhoneCall() {
        this.call_id = null;
        this.my_leg_uuid = null;
        this.other_leg_uuid = null;
        this.terminated = false;
        this.incoming = false;
        this.answered = false;
    }
    PhoneCall.prototype = Object.create(Backbone.Events);
    PhoneCall.prototype.constructor = PhoneCall;
    declareProtoMethod(PhoneCall, 'answer');
    declareProtoMethod(PhoneCall, 'terminate');
    declareProtoMethod(PhoneCall, 'hold');
    declareProtoMethod(PhoneCall, 'unhold');
    declareProtoMethod(PhoneCall, 'isOnHold');
    declareProtoMethod(PhoneCall, 'isReallyOnHold');
    declareProtoMethod(PhoneCall, 'sendKey');
    declareProtoMethod(PhoneCall, 'transferCall');
    // Not implemented yet: 'inviteParticipant');
    declareProtoMethod(PhoneCall, 'setInputVolume');
    declareProtoMethod(PhoneCall, 'getInputVolume');
    declareProtoMethod(PhoneCall, 'setVoiceVolume');
    declareProtoMethod(PhoneCall, 'getVoiceVolume');

    function PhoneSounds() {
    }
    declareProtoMethod(PhoneSounds, 'destroy');
    declareProtoMethod(PhoneSounds, 'setVolume');
    declareProtoMethod(PhoneSounds, 'getVolume');
    declareProtoMethod(PhoneSounds, 'setMuted');
    declareProtoMethod(PhoneSounds, 'addToPhone');
    declareProtoMethod(PhoneSounds, 'removeFromPhone');
    declareProtoMethod(PhoneSounds, 'addToPhoneCall');
    declareProtoMethod(PhoneSounds, 'removeFromPhoneCall');

    var exports = {};
    declareMethod(exports, 'overrideGetUserMedia');
    declareMethod(exports, 'wrappedGetUserMedia');

    exports.Phone = Phone;
    exports.PhoneCall = PhoneCall;
    exports.PhoneSounds = PhoneSounds;
    return exports;
});
